#include "io.h"

#include <QDir>

using namespace std;
using namespace cv;

Calibration::IO::IO(QObject *parent) :
    QObject(parent)
{

}


bool Calibration::IO::saveParameters(const string& file_path, const vector<Parameter>& parameters){
    if(file_path.size()==0){
        ERROR("Invalid path to save parameters: "+QString::fromStdString(file_path));
        return false;
    }

    ofstream file(file_path, ofstream::out | ofstream::trunc);
    if(file.is_open()){
        for(const Parameter& param : parameters){
            switch (param.type) {
            case VAR_INT:
                file << to_string(*(int*)param.pointer) << "\n";
                break;
            case VAR_FLOAT:
                file << to_string(*(float*)param.pointer) << "\n";
                break;
            case VAR_DOUBLE:
                file << to_string(*(double*)param.pointer) << "\n";
                break;
            case VAR_STRING:
                file << *(string*)param.pointer << "\n";
                break;
            default:
                break;
            }
        }

        file.close();
    }
    else{
        ERROR("Unable to open or create file "+QString::fromStdString(file_path)+". Parameters could not be written.");
        return false;
    }

    return true;
}

bool Calibration::IO::loadParameters(const string& file_path, vector<Parameter>* parameters){
    if(file_path.size()==0){
        ERROR("Invalid path to load parameters: "+QString::fromStdString(file_path));
        return false;
    }

    ifstream file(file_path);
    if(file.is_open()){
        for(Parameter& param : *parameters){
            switch (param.type) {
            case VAR_INT:
                file >> *(int*)param.pointer;
                break;
            case VAR_FLOAT:
                file >> *(float*)param.pointer;
                break;
            case VAR_DOUBLE:
                file >> *(double*)param.pointer;
                break;
            case VAR_STRING:
                file >> *(string*)param.pointer;
                break;
            default:
                break;
            }
        }

        file.close();
    }
    else{
        ERROR("Unable to open file "+QString::fromStdString(file_path)+". Parameters could not be written.");
        return false;
    }

    return true;
}

bool Calibration::IO::loadRigPositions(const string& file_path, int* num_rig_positions, map<int,pairf>* rig_positions){
    if(rig_positions == nullptr || num_rig_positions == nullptr){
        ERROR("No rig position vector available to write the positions to.");
        return false;
    }
    if(file_path.size()==0){
        rig_positions->clear();
        ERROR("Invalid path to load rig positions file: "+QString::fromStdString(file_path));
        return false;
    }

    rig_positions->clear();
    *num_rig_positions = 0;

    ifstream file(file_path);
    if(file.is_open()){
        string line;
        int first, second;
        int rig_position;
        float x_factor,y_factor;
        pairf zero_position = pairf(-1.0,-1.0);
        while(getline(file,line)){
            //assuming every line contains "rig_position_number x_factor y_factor"
            first = line.find(" ",0);
            second = line.find(" ",first+1);
            if(first==-1 || second==-1){
                ERROR("The rig positions file has the wrong layout. Position number, x_factor, and y_factor should be separated by spaces.");
                rig_positions->clear();
                return false;
            }
            try{
                rig_position = atoi(line.substr(0,first).c_str());
                first++;
                x_factor = atof(line.substr(first, second-first).c_str());
                second++;
                y_factor = atof(line.substr(second, line.size()-second).c_str());
            }
            catch(...){
                ERROR("Unable to convert factors to float. The rig positions file contains invalid characters.");
                rig_positions->clear();
                return false;
            }
            rig_positions->insert(make_pair(rig_position,pairf(x_factor,y_factor)));

            if(zero_position.first == -1.0){
                //save initial rig position
                zero_position = pairf(x_factor,y_factor);
            }
            else if(*num_rig_positions == 0 && x_factor == zero_position.first && y_factor == zero_position.second){
                //if current position equals initial position, set num_rig_positions
                *num_rig_positions = rig_positions->size()-1;
            }
        }
        if(*num_rig_positions == 0){
            *num_rig_positions = rig_positions->size();
        }
    }
    else{
        ERROR("Unable to open file "+QString::fromStdString(file_path)+". Parameters could not be written.");
        return false;
    }

    return true;
}

bool Calibration::IO::createImageList(const string& dir_path, const map<int,pairf>& rig_positions, Files* files, Detection* detection, vector<Camera>* cameras){
    if(files == nullptr || detection == nullptr){
        ERROR("No file list or detection struct given.");
        return false;
    }

    files->file_idx.clear();
    files->file_paths.clear();

    //check if image_dir exists
    if(dir_path.size() == 0){
        ERROR("No image dir specified.");
        return false;
    }
    QDir dir(QString::fromStdString(dir_path));
    if(!dir.exists()){
        ERROR("Image dir does not exist.");
        return false;
    }

    //get list of image files and wirte list to imagelist.xml
    QStringList file_list = dir.entryList({"*.png","*.jpeg","*.jpg","*.bmp","*.tif","*.tiff","*.mat"});
    vector<string> camera_ids;
    vector<string> rig_pos_ids;
    vector<vector<int>> image_idx_list; //since we do not enforce continuous numbering, this list saves the indices corresponding to the ids above

    int num_rig_pos_ids = 0;

    for(int i = 0; i < file_list.size(); i++){
        const QString& file = file_list[i];
        const string file_name = file.toStdString();
        const string file_path = dir_path+"/"+file_name;
        files->file_paths.push_back(file_path);

        //analyze file_name - assume naming according to $CAMID_$RIGPOSID.$FILETYPE
        vector<string> image_ids;
        if(!splitString(file_name,&image_ids,DELIM_BOTH) || image_ids.size() < 2){
            WARN("File name could not be analyzed correctly: "+file);
            continue;
        }

        //calculate image idx
        vector<int> image_idx;
        int cam_idx = -1;
        if(cameras->size() > 0 && cameras->back().id.compare(image_ids[0]) != 0){
            for(unsigned int j = 0; j < cameras->size(); j++){
                if(cameras->at(j).id.compare(image_ids[0]) == 0){
                    cam_idx = int(j);
                }
            }
        }
        else{
            cam_idx = cameras->size()-1;
        }

        if(cam_idx == -1 || cam_idx == (int)camera_ids.size()){//new cam found
            detection->num_cams++;

            //create new camera
            Camera cam;
            cam.id = image_ids[0];

            //load image in order to get resolution
            Mat image;
            if(file_path.substr(file_path.size()-3).compare("mat") == 0){
                readFloatImage(file_path, &image);
            }
            else{
                image = imread(file_path);
            }
            cam.resolution_x = image.cols;
            cam.resolution_y = image.rows;
            cameras->push_back(cam);
            camera_ids.push_back(image_ids[0]);

            cam_idx = cameras->size()-1;

            detection->detected_cam_ids.push_back(image_ids[0]);
        }

        image_idx.push_back(cam_idx);

        int rig_pos_idx = distance(rig_pos_ids.begin(),find(rig_pos_ids.begin(),rig_pos_ids.end(),image_ids[1]));

        if(rig_pos_idx == -1 || rig_pos_idx == (int)rig_pos_ids.size()){//new rig position id found
            num_rig_pos_ids++;
            rig_pos_ids.push_back(image_ids[1]);
            image_idx.push_back(rig_pos_ids.size()-1);
        }
        else{
            image_idx.push_back(rig_pos_idx);
        }

        image_idx_list.push_back(image_idx);
    }

    detection->num_pattern_positions = int(float(num_rig_pos_ids) / float(detection->num_rig_positions));

    //fill the files->file_idx lookup table for cam/pattern position/rig position triples
    vector<int> indices_per_pattern(detection->num_rig_positions, -1);
    vector_2D<int> indices_per_cam;
    for(int i = 0 ; i < detection->num_pattern_positions; i++){
        indices_per_cam.push_back(indices_per_pattern);
    }
    for(int i = 0 ; i < detection->num_cams; i++){
        files->file_idx.push_back(indices_per_cam);
    }

    for(size_t i = 0 ; i < image_idx_list.size(); i++){
        if(image_idx_list[i].size()>0){
            files->file_idx[image_idx_list[i][0]][(int)((float)(image_idx_list[i][1])/(float)(detection->num_rig_positions))][image_idx_list[i][1] % detection->num_rig_positions] = i;
        }
    }

    //fill the detection->rig_positions vector
    detection->rig_positions.clear();
    if(rig_positions.size() != 0){
        pairf first_position;
        bool first_found = false;
        for(int rig_idx = 0; rig_idx < detection->num_rig_positions; rig_idx++){
            auto iterator = rig_positions.find(rig_idx);
            if(iterator != rig_positions.end()){
                if(!first_found){
                    //save first position
                    first_position = iterator->second;
                    first_found = true;
                }
                //calculate position relative to first rig position
                pairf relative_position = iterator->second;
                relative_position.first -= first_position.first;
                relative_position.second -= first_position.second;
                detection->rig_positions.push_back(relative_position);
            }
            else{
                ERROR("Rig position conversion failed.");
                return false;
            }
        }
    }

    return true;

}

bool Calibration::IO::saveCorrespondences(const string& file_path, const vector_3D<Feature_List>& correspondences, const vector<pairf>& rig_positions, const std::vector<Camera>& cameras){

    ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Could not correspondences file.");
        return false;
    }


    if(correspondences.size() == 0 || correspondences[0].size() == 0 || correspondences[0][0].size() == 0){
        ERROR("No points to save");
        return false;
    }

    if(cameras.size() != correspondences.size()){
        ERROR("Number of cameras does not equal the correspondence table size in first dimension.");
        return false;
    }

    //write file header
    file << "Size " << correspondences.size() << " " << correspondences[0].size() << " " << correspondences[0][0].size() <<"\n";

    //write resolutions
    file << "Resolutions\n";
    for(size_t cam_idx = 0; cam_idx < cameras.size(); cam_idx++){
        file << cameras[cam_idx].id << " " << cameras[cam_idx].resolution_x << " " << cameras[cam_idx].resolution_y << "\n";
    }

    //write rig positions
    file << "Rig_Positions\n";
    if(rig_positions.size() == 0){
        file << "0 0" << "\n";
    }
    else{
        for(size_t rig_idx = 0; rig_idx < rig_positions.size(); rig_idx++){
            file << rig_positions[rig_idx].first << " " << rig_positions[rig_idx].second << "\n";
        }
    }

    //write correspondences to file
    for(size_t cam_idx = 0; cam_idx < correspondences.size(); cam_idx++){
        for(size_t pattern_idx = 0; pattern_idx < correspondences[0].size(); pattern_idx++){
            for(size_t rig_idx = 0; rig_idx < correspondences[0][0].size(); rig_idx++){
                file << "Image " << cameras[cam_idx].id << " " << pattern_idx << " " << rig_idx << "\n";
                for(size_t point_idx = 0; point_idx < correspondences[cam_idx][pattern_idx][rig_idx].size(); point_idx++){
                    const Feature_Point& p = correspondences[cam_idx][pattern_idx][rig_idx][point_idx];
                    file << p.p[0] << " " << p.p[1] << " " << p.id << " " << p.corr[0] << " " << p.corr[1] << " " << p.corr[2];
                    if(point_idx+1 == correspondences[cam_idx][pattern_idx][rig_idx].size()){
                        file << "\n";
                    }
                    else{
                        file << " ";
                    }
                }
            }
        }
    }

    STATUS("Detected points saved to file "+QString::fromStdString(file_path));
    return true;
}

bool Calibration::IO::loadCorrespondences(const string& file_path, Detection* detection, Estimation* estimation){

    ifstream file(file_path);
    if(!file.is_open()){
        ERROR("Unable to open correspondences file.");
        return false;
    }

    detection->clear();

    string line;
    int num_of_cams = -1;
    int num_of_pattern_pos = -1;
    int num_of_rig_pos = -1;

    int cam_idx = -1;
    int pattern_idx = -1;
    int rig_idx = -1;
    string cam_id;

    int read_mode = READ_FEATURES;

    while(getline(file,line)){
        //read descriptor line
        if(line.substr(0,4).compare("Size") == 0){
            std::istringstream stream(line.substr(5));
            stream >> num_of_cams;
            stream >> num_of_pattern_pos;
            stream >> num_of_rig_pos;

            detection->num_cams = num_of_cams;
            detection->num_pattern_positions = num_of_pattern_pos;
            detection->num_rig_positions = num_of_rig_pos;

            //init tables
            detection->correspondences = vector_3D<Feature_List>(num_of_cams,vector_2D<Feature_List>(num_of_pattern_pos,vector<Feature_List>(num_of_rig_pos,Feature_List(0))));
        }
        else if(line.substr(0,5).compare("Image") == 0){
            read_mode = READ_FEATURES;

            std::istringstream stream(line.substr(6));
            stream >> cam_id;
            stream >> pattern_idx;
            stream >> rig_idx;

            Camera cam;
            cam.id = cam_id;
            cam_idx = estimation->addCamera(cam,SET_CAMERA_ALL,false);

            if(find(detection->detected_cam_ids.begin(),detection->detected_cam_ids.end(),cam_id) == detection->detected_cam_ids.end()){
                detection->detected_cam_ids.push_back(cam_id);
            }

            if(pattern_idx < 0 || pattern_idx+1 > num_of_pattern_pos || rig_idx < 0 || rig_idx+1 > num_of_rig_pos){
                detection->clear();
                ERROR("The corner file is not correctly formatted.");
                return false;
            }

            if(num_of_rig_pos > 1){
                string val;
                stream >> val;
            }
        }
        else if(line.compare("Resolutions") == 0){
            read_mode = READ_RESOLUTION;
        }
        else if(line.compare("Rig_Positions") == 0){
            read_mode = READ_RIG_POSITIONS;
        }
        else if(read_mode == READ_FEATURES){
            //read feature point line
            std::istringstream stream(line);
            Feature_Point feature;
            while (!stream.eof()){
                stream >> feature.p[0];
                stream >> feature.p[1];
                stream >> feature.id;
                stream >> feature.corr[0];
                stream >> feature.corr[1];
                stream >> feature.corr[2];
                detection->correspondences[cam_idx][pattern_idx][rig_idx].push_back(feature);
            }
        }
        else if(read_mode == READ_RESOLUTION){
            //read feature point line
            std::istringstream stream(line);
            Camera cam;
            stream >> cam.id;
            stream >> cam.resolution_x;
            stream >> cam.resolution_y;

            estimation->addCamera(cam, SET_CAMERA_RESOLUTION);
        }
        else if(read_mode == READ_RIG_POSITIONS){
            //read rig position
            std::istringstream stream(line);
            pairf position;
            stream >> position.first;
            stream >> position.second;
            detection->rig_positions.push_back(position);
        }
    }

    //convert saved Feature_Points to opencv Mats
    return detection->convertCorrespondencesToMat();
}

bool Calibration::IO::saveCameras(const string& file_path, const vector<Camera>& cameras){

    ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Could not write camera file.");
        return false;
    }

    //add description
    file << "$CamID $Width $Height $CamType\n";
    file << "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3\n";
    file << "$K matrix rowwise\n";
    file << "$Pose matrix rowwise\n\n";

    for(const Camera& cam : cameras){
        file << "Cam "<< cam.id << " " << cam.resolution_x << " " << cam.resolution_y << " " << cam.type << "\n";
        const cv::Mat distortion = cam.getDistortion();
        saveMat(&file, distortion);
        file << "\n";
        saveMat(&file, cam.K);
        file << "\n";
        saveMat(&file, cam.pose);
        file << "\n";
    }

    return true;
}

bool Calibration::IO::loadCameras(const string& file_path, vector<Camera>* cameras){

    ifstream file(file_path);
    if(!file.is_open()){
        ERROR("Unable to open camera file.");
        return false;
    }

    cameras->clear();
    string line;

    while(getline(file,line)){
        if(line.size() > 3){
            if(line.at(0) == '$'){
                continue;
            }
            else if(line.substr(0,3).compare("Cam") == 0){
                Camera cam;
                std::istringstream stream(line.substr(3));
                stream >> cam.id >> cam.resolution_x >> cam.resolution_y >> cam.type;

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                cv::Mat distortion_mat = cv::Mat(1,5,CV_64F);
                loadMat(&stream, &distortion_mat);
                cam.setDistortion(distortion_mat);

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                loadMat(&stream, &cam.K);

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                loadMat(&stream, &cam.pose);

                cameras->push_back(cam);
            }
        }
    }

    return true;
}

bool Calibration::IO::saveTransformations(const string& file_path, const Estimation& estimation){
    if(estimation.rotations.size() == 0 || estimation.rotations[0].size() == 0 || estimation.rotations[0][0].size() == 0){
        ERROR("No transformations to save.");
        return false;
    }

    ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Could not write transformations file.");
        return false;
    }

    file << "Size " << estimation.rotations.size() << " " << estimation.rotations[0].size() << " " << estimation.rotations[0][0].size() << "\n";

    for(size_t cam_idx = 0; cam_idx < estimation.rotations.size(); cam_idx++){
        for(size_t pattern_idx = 0; pattern_idx < estimation.rotations[0].size(); pattern_idx++){
            for(size_t rig_idx = 0; rig_idx < estimation.rotations[0][0].size(); rig_idx++){
                Mat rod = estimation.rotations[cam_idx][pattern_idx][rig_idx];
                Mat t = estimation.translations[cam_idx][pattern_idx][rig_idx];

                file << "Image " << estimation.cams[cam_idx].id << " " << pattern_idx << " " << rig_idx << "\n";
                if(rod.total() > 1){
                    file << rod.at<double>(0,0);
                    for(int i=1; i<3; i++){
                        file << " " << to_string(rod.at<double>(i,0));
                    }
                    for(int i=0; i<3; i++){
                        file << " " << to_string(t.at<double>(i,0));
                    }
                    file << "\n";
                }
                else{
                    file << "\n";
                }
            }
        }
    }

    return true;
}

bool Calibration::IO::loadTransformations(const string& file_path, Estimation* estimation){
    ifstream file(file_path);
    if(!file.is_open()){
        ERROR("Unable to open transform file.");
        return false;
    }
    if(estimation->cams.size() == 0){
        ERROR("Please load the cameras first!");
        return false;
    }

    estimation->rotations.clear();
    estimation->translations.clear();

    int num_cams, num_pattern, num_rig;

    int cam_idx = -1;
    int pattern_idx = -1;
    int rig_idx = -1;
    string cam_id;

    string line;
    while(getline(file,line)){
        if(line.substr(0,4).compare("Size") == 0){
            std::istringstream stream(line.substr(5));
            stream >> num_cams;
            stream >> num_pattern;
            stream >> num_rig;

            num_cams = max(num_cams, int(estimation->cams.size()));

            //initialize tables
            estimation->rotations = vector_3D<Mat>(num_cams,vector_2D<Mat>(num_pattern,vector<Mat>(num_rig,Mat(0,0,CV_64F))));
            estimation->translations = vector_3D<Mat>(num_cams,vector_2D<Mat>(num_pattern,vector<Mat>(num_rig,Mat(0,0,CV_64F))));
            estimation->transforms = vector_3D<Mat>(num_cams,vector_2D<Mat>(num_pattern,vector<Mat>(num_rig,Mat(0,0,CV_64F))));

        }
        else if(line.substr(0,5).compare("Image") == 0){
            //read indices
            std::istringstream stream(line.substr(5));
            stream >> cam_id;
            stream >> pattern_idx;
            stream >> rig_idx;

            //search for cam_id in existing cameras
            Camera cam;
            cam.id = cam_id;
            cam_idx = estimation->addCamera(cam,SET_CAMERA_ALL,false);
        }
        else if(line.size() != 0){
            //read transformation data
            vector<double> rod_data, t_data;
            double val;
            std::istringstream stream(line);
            for(int i=0; i<3;i++){
                stream >> val;
                rod_data.push_back(val);
            }
            for(int i=0; i<3;i++){
                stream >> val;
                t_data.push_back(val);
            }

            estimation->rotations[cam_idx][pattern_idx][rig_idx] = Mat(3,1,CV_64F,rod_data.data()).clone();
            estimation->translations[cam_idx][pattern_idx][rig_idx] = Mat(3,1,CV_64F,t_data.data()).clone();
        }
    }

    return true;
}


////////////////////////////////////////     HELPER      ////////////////////////////////////////


bool Calibration::IO::splitString(const string& str, vector<string>* parts, const int delim_code){
    if(str.size() == 0 || parts == nullptr){
        return false;
    }

    parts->clear();

    vector<char> delimiters;
    if(delim_code == DELIM_MINUS || delim_code == DELIM_BOTH){
        delimiters.push_back('-');
    }
    if(delim_code == DELIM_UNDERSCORE || delim_code == DELIM_BOTH){
        delimiters.push_back('_');
    }

    int last_delim_position = -1;
    string substr;
    for(size_t i = 0; i < str.size(); i++){
        const char& c = str[i];
        for(char& delimiter : delimiters){
            if(c == delimiter){
                substr = str.substr(last_delim_position+1,i);
                parts->push_back(substr);

                last_delim_position = i;
                break;
            }
        }
        if(c == '.'){
            substr = str.substr(last_delim_position+1,i-last_delim_position-1);
            parts->push_back(substr);
            break;
        }
    }

    return true;
}

bool Calibration::IO::saveMat(ofstream* file, const Mat& mat){
    for(int row = 0; row < mat.rows; row++){
        for(int col = 0; col < mat.cols; col++){

            if(mat.type() == CV_64F){
                *file << mat.at<double>(row,col);
            }
            else if(mat.type() == CV_32F){
                *file << mat.at<float>(row,col);
            }
            else if(mat.type() == CV_8U){
                *file << mat.at<uchar>(row,col);
            }

            if(row == mat.rows-1 && col == mat.cols-1){
                break;
            }
            else{
                *file<<" ";
            }
        }
    }

    return true;
}

bool Calibration::IO::loadMat(istringstream* file, Mat* mat){
    for(int row = 0; row < mat->rows; row++){
        for(int col = 0; col < mat->cols; col++){

            if(mat->type() == CV_64F){
                *file >> mat->at<double>(row,col);
            }
            else if(mat->type() == CV_32F){
                *file >> mat->at<float>(row,col);
            }
            else if(mat->type() == CV_8U){
                *file >> mat->at<uchar>(row,col);
            }
        }
    }

    return true;
}

bool Calibration::IO::readFloatImage(const string file_path, Mat* image){
    if(file_path.size() == 0){
        ERROR("No valid file path given.");
        return false;
    }
    if(image == nullptr){
        ERROR("No valid image pointer given.");
        return false;
    }

    //check file type
    std::string type = file_path.substr(file_path.size()-3);
    if(type.compare("mat") == 0){
        std::ifstream file(file_path);
        if(file.is_open()){
            //read matrix dimensions
            //count items per row
            std::string line;
            std::getline(file,line);
            std::istringstream line_(line);
            int width = std::count(std::istreambuf_iterator<char>(line_), std::istreambuf_iterator<char>(),' ');
            //count rows
            int height = std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(),'\n')+1;
            //reset stream
            file.clear();
            file.seekg(0, std::ios::beg);

            if(width<=0 || height<=0){
                ERROR("The file "+QString::fromStdString(file_path)+" does not contain sufficient data.");
                return false;
            }

            //read mat file values
            vector<float> data(height*width);
            for(int i=0;i<width*height;i++){
                file >> data[i];
            }
            *image = Mat(height,width,CV_32F,data.data()).clone();

        }
        else{
            ERROR("Unable to open file "+QString::fromStdString(file_path));
            return false;
        }
    }
    else if(type.compare("png") == 0){
        cv::Mat temp = cv::imread(file_path,cv::IMREAD_UNCHANGED);
        if(temp.type() != CV_8UC4){
            ERROR("Chosen image does not have 4 channels.");
            return false;
        }
        *image = Mat(temp.rows,temp.cols,CV_32F,temp.data);
    }
    else if(type.compare("exr") == 0){
        ERROR("exr is currently not supported.");
        return false;
    }
    else{
        ERROR("Invalid depth file format.");
        return false;
    }

    return true;
}
