#include "pattern_operations.h"
#include "io.h"

#include <opencv2/aruco/charuco.hpp>
#include <opencv2/aruco/dictionary.hpp>
#include <opencv2/ccalib/randpattern.hpp>

using namespace std;
using namespace cv;


Calibration::Pattern_Operations::Pattern_Operations(QObject *parent) : QObject(parent){

}

bool Calibration::Pattern_Operations::createChArUcoPattern(const int width, const int height, const ChArUco_Params& charuco_params, cv::Mat* pattern){

    //check if number of needed charuco markers is smaller than dictionary size
    vector<int> dict_size = {50,100,250,1000};
    if(width*height/2 > dict_size[charuco_params.dict%4]){
        ERROR("Unable to create marker board - dictionary size too small.");
        return false;
    }

    //get dictionary

    Ptr<aruco::Dictionary> dict = aruco::getPredefinedDictionary(charuco_params.dict);
    //create board
    Ptr<aruco::CharucoBoard> board;
    try{
        board = aruco::CharucoBoard::create(width, height, charuco_params.square_size, charuco_params.marker_to_square_ratio*charuco_params.square_size, dict);
    }
    catch(...){
        ERROR("Unable to create marker board.");
        return false;
    }
    //calculate optimal draw size to prevent interpolation which results in blurred edges
    int marker_size_px = 4+(int)((float)charuco_params.dict / 4.0);
    float px_per_square = 1.0/charuco_params.marker_to_square_ratio*(float)(marker_size_px+2*charuco_params.marker_border_pixel);
    float px_border = px_per_square*charuco_params.border_size;

    while(px_per_square-(float)((int)px_per_square)>0 && px_per_square<500 && px_border-(float)((int)px_border)>0){
        px_per_square*=10.0;
        px_border*=10.0;
    }

    int draw_width = width*px_per_square+2*(int)px_border;
    int draw_height = height*px_per_square+2*(int)px_border;

    //draw board to image
    try{
        board->draw( cv::Size(draw_width, draw_height), *pattern, (int)px_border, charuco_params.marker_border_pixel);
    }
    catch(...){
        ERROR("Unable to draw marker board.");
        return false;
    }

    if(pattern->rows>0 && pattern->cols>0){
        STATUS("Pattern successfully generated.");
    }
    else{
        ERROR("No pattern generated.");
        return false;
    }

    return true;
}

bool Calibration::Pattern_Operations::createRandomPattern(const int width, const int height, cv::Mat* pattern){

    cv::randpattern::RandomPatternGenerator generator(width, height);
    generator.generatePattern();
    *pattern = generator.getPattern();

    if(pattern->rows>0 && pattern->cols>0){
        STATUS("Pattern successfully generated.");
    }
    else{
        ERROR("No pattern generated.");
        return false;
    }

    return true;
}

bool Calibration::Pattern_Operations::detectChArUcoPatterns(const int width, const int height, const ChArUco_Params& charuco_params, const Files& files, Detection* detection){
    if(detection == nullptr){
        ERROR("No list given to save detected points in.");
        return false;
    }
    detection->correspondences.clear();

    //get dictionary
    const Ptr<aruco::Dictionary> dict = aruco::getPredefinedDictionary(charuco_params.dict);
    //create board
    Ptr<aruco::CharucoBoard> board;
    try{
        board = aruco::CharucoBoard::create(width, height, charuco_params.square_size, charuco_params.marker_to_square_ratio*charuco_params.square_size, dict);
    }
    catch(...){
        ERROR("Unable to create marker board.");
        return false;
    }

    //function for calculating the image coordinates of a charuco corner
    auto charucoIdToImageCoordinates = [](int width, int id, double scale){
        double x = (double)(id % (width-1));
        double y = (double)((int)((double)(id) / (double)(width-1)));
        return Vec3d(scale*x,scale*y,0.0);
    };

    //io object for loading of float images
    IO io;

    for(int cam_idx = 0; cam_idx < detection->num_cams; cam_idx++){
        vector_3D<Feature_Point> features_for_cam;
        for(int pat_idx = 0; pat_idx < detection->num_pattern_positions; pat_idx++){
            vector<vector<Feature_Point>> features_for_pattern;
            for(int rig_idx = 0; rig_idx < detection->num_rig_positions; rig_idx++){
                vector<Feature_Point> features_for_rigpos;
                int file_path_idx = files.file_idx[cam_idx][pat_idx][rig_idx];
                if(file_path_idx != -1){
                    //load image
                    Mat image;
                    string file_path = files.file_paths[file_path_idx];
                    if(file_path.substr(file_path.size()-3).compare("mat") == 0){
                        //load float image (e.g. depth images)
                        if(!io.readFloatImage(file_path, &image)){
                            ERROR("Could not read float image "+QString::fromStdString(file_path));
                            return false;
                        }
                        else{
                            //scale and convert float image to 1 channel uchar image
                            cv::log(image,image);
                            image = 255.0*image/std::log(65536.0);
                            image.convertTo(image,CV_8UC1);
//                            cv::imshow("IR image2", image);
//                            cv::waitKey(0);
                        }
                    }
                    else{
                        //load color image
                        image = imread(file_path);
                    }

                    //find charuco markers
                    vector<vector<Point2f>> marker_corners;
                    vector<int> marker_ids;
                    aruco::detectMarkers(image,dict,marker_corners,marker_ids);

                    //find associated corners
                    if(marker_ids.size()>0){
                        vector<Vec2f> corners;
                        vector<int> ids;
                        aruco::interpolateCornersCharuco(marker_corners,marker_ids,image,board,corners,ids);

                        //save feature points if at least 8 are found and corners are not in one line
                        if(ids.size()>7){
                            //check if corners are in one line by calculating angles -> degenerated cases are unusable for transformation estimations
                            bool degenerated = true;
                            for(size_t i = 0; i<ids.size() && degenerated; i++){
                                Vec2f& a = corners[i];
                                for(size_t j = 0; j<ids.size() && degenerated; j++){
                                    if(j != i){
                                        Vec2f& b = corners[j];
                                        for(size_t k = 0; k<ids.size() && degenerated; k++){
                                            if(k != i && k != j){
                                                Vec2f& c = corners[k];
                                                Vec2f ab = b-a;
                                                Vec2f ac = c-a;
                                                ab /= norm(ab);
                                                ac /= norm(ac);
                                                float angle = 180.0*acos(ab[0]*ac[0]+ab[1]*ac[1])/M_PI;
                                                if(std::fabs(angle)>45 && std::fabs(angle)<135){
                                                    degenerated = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if(!degenerated){

                                //check if the points actually define a (distorted) regular grid - this is necessary since interpolateCornersCharuco
                                //does not filter the detected markers according to their plausability
                                bool is_grid = true;
                                vector<Vec2d> pattern_corners;
                                for(size_t i = 0; i<ids.size(); i++){
                                    Vec3d pattern_corner = charucoIdToImageCoordinates(width,ids[i],charuco_params.square_size);
                                    pattern_corners.push_back(Vec2d(pattern_corner[0],pattern_corner[1]));
                                }
                                Mat H = findHomography(corners,pattern_corners,RANSAC);
                                H.convertTo(H,CV_64F);
                                for(size_t i = 0; i<ids.size(); i++){
                                    Mat projected = H*Mat(Vec3d(corners[i][0],corners[i][1],1.0));
                                    Vec2d diff_2d = Vec2d(projected.at<double>(0,0)/projected.at<double>(2,0),projected.at<double>(1,0)/projected.at<double>(2,0)) - pattern_corners[i];
                                    if(cv::norm(diff_2d) > 20){
                                        is_grid = false;
                                        break;
                                    }
                                }

                                //save points
                                if(is_grid){
                                    for(size_t i = 0; i<ids.size(); i++){
                                        Vec2d corner(corners[i]);
                                        features_for_rigpos.push_back(Feature_Point(corner,ids[i],charucoIdToImageCoordinates(width,ids[i],charuco_params.square_size)));
                                    }
                                }
                                else{
                                    WARN("Pattern in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx)+" was rejected - not a grid.");
//                                    aruco::drawDetectedCornersCharuco(image,corners,ids,Scalar(0,0,255));
//                                    aruco::drawDetectedMarkers(image,marker_corners,marker_ids,Scalar(0,255,0));
//                                    cv::imshow("Test",image);
//                                    cv::waitKey(0);
                                }
                            }
                            else{
                                WARN("Pattern in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx)+" was rejected - degenerated case.");
                            }
                        }
                        STATUS("Found "+QString::number(features_for_rigpos.size())+" features in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx));
                    }
                    else{
                        STATUS("Found 0 features in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx));
                    }
                }
                features_for_pattern.push_back(features_for_rigpos);
            }
            features_for_cam.push_back(features_for_pattern);
        }
        detection->correspondences.push_back(features_for_cam);
    }

    return convertFeaturelistsToMat(detection);
}

bool Calibration::Pattern_Operations::detectRandomPatterns(const int width, const int height, const cv::Mat& pattern, const Files& files, Detection* detection){
    if(detection == nullptr){
        ERROR("No list given to save detected points in.");
        return false;
    }
    detection->correspondences.clear();

    cv::randpattern::RandomPatternCornerFinder finder(width, height, 20);
    finder.loadPattern(pattern);
    Mat image;
    int detection_counter = 0;

    for(int cam_idx = 0; cam_idx < detection->num_cams; cam_idx++){
        vector_3D<Feature_Point> features_for_cam;
        vector<Mat> object_points;
        vector<Mat> image_points;
        for(int pat_idx = 0; pat_idx < detection->num_pattern_positions; pat_idx++){
            vector_2D<Feature_Point> features_for_pattern;
            for(int rig_idx = 0; rig_idx < detection->num_rig_positions; rig_idx++){
                vector<Feature_Point> features_for_rigpos;
                int file_path_idx = files.file_idx[cam_idx][pat_idx][rig_idx];
                if(file_path_idx != -1){
                    //load image
                    image = imread(files.file_paths[file_path_idx]);
                    cv::cvtColor(image,image,COLOR_BGR2GRAY);

                    //find random pattern features
                    vector<Mat> image_vec = {image};
                    finder.computeObjectImagePoints(image_vec);

                    object_points = finder.getObjectPoints();
                    image_points = finder.getImagePoints();

                    //save feature points
                    if(object_points.size() > (size_t) detection_counter){
                        object_points.back().convertTo(object_points.back(),CV_64FC3);
                        image_points.back().convertTo(image_points.back(),CV_64FC2);

                        detection_counter++;
                        STATUS("Found "+QString::number(object_points.back().rows)+" features in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx));
                        for(int feat_idx = 0; feat_idx < image_points.back().rows; feat_idx++){
                            object_points.back().at<Vec3d>(feat_idx,0)[2]=0.0;
                            features_for_rigpos.push_back(Feature_Point(image_points.back().at<Vec2d>(feat_idx,0),-1,object_points.back().at<Vec3d>(feat_idx,0)));
                        }
                    }
                    else{
                        STATUS("Found 0 features in image "+QString::number(cam_idx)+"_"+QString::number(pat_idx));
                    }
                }
                features_for_pattern.push_back(features_for_rigpos);
            }
            features_for_cam.push_back(features_for_pattern);
        }
        detection->correspondences.push_back(features_for_cam);
    }

    return convertFeaturelistsToMat(detection);
}

bool Calibration::Pattern_Operations::convertFeaturelistsToMat(Detection* detection){
    detection->object_points.clear();
    detection->image_points.clear();

    //convert detected Feat_Points to opencv mats
    for(int cam_idx = 0; cam_idx < detection->num_cams; cam_idx++){
        vector_2D<cv::Mat> object_points_for_cam;
        vector_2D<cv::Mat> image_points_for_cam;

        for(int pat_idx = 0; pat_idx < detection->num_pattern_positions; pat_idx++){
            vector<cv::Mat> object_points_for_pattern;
            vector<cv::Mat> image_points_for_pattern;

            for(int rig_idx = 0; rig_idx < detection->num_rig_positions; rig_idx++){

                cv::Mat object_points_for_rig(0,0,CV_64FC3);
                cv::Mat image_points_for_rig(0,0,CV_64FC2);

                if(detection->correspondences[cam_idx][pat_idx][rig_idx].size() != 0){
                    object_points_for_rig = cv::Mat(detection->correspondences[cam_idx][pat_idx][rig_idx].size(),1,CV_64FC3);
                    image_points_for_rig = cv::Mat(detection->correspondences[cam_idx][pat_idx][rig_idx].size(),1,CV_64FC2);

                    for(size_t point_idx = 0; point_idx < detection->correspondences[cam_idx][pat_idx][rig_idx].size(); point_idx++){
                        const Feature_Point& p = detection->correspondences[cam_idx][pat_idx][rig_idx][point_idx];
                        object_points_for_rig.at<Vec3d>(point_idx,0) = p.corr;
                        image_points_for_rig.at<Vec2d>(point_idx,0) = p.p;
                    }
                }
                object_points_for_pattern.push_back(object_points_for_rig);
                image_points_for_pattern.push_back(image_points_for_rig);
            }
            object_points_for_cam.push_back(object_points_for_pattern);
            image_points_for_cam.push_back(image_points_for_pattern);
        }
        detection->object_points.push_back(object_points_for_cam);
        detection->image_points.push_back(image_points_for_cam);
    }

    return true;
}
