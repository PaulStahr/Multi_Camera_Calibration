QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

TARGET = Multi_Camera_Calibration
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        main_window.cpp \
    calibration_widget.cpp \
    pattern_operations.cpp \
    io.cpp \
    data.cpp \
    logger.cpp \
    optimization.cpp \
    graphics_view.cpp \
    calibration_interface.cpp \
    cam_params_widget.cpp \
    matrix_edit.cpp

HEADERS += \
        main_window.h \
    calibration_widget.h \
    pattern_operations.h \
    io.h \
    data.h \
    logger.h \
    optimization.h \
    helper.h \
    graphics_view.h \
    calibration_interface.h \
    cam_params_widget.h \
    matrix_edit.h

FORMS += \
        main_window.ui \
    calibration_widget.ui \
    cam_params_widget.ui


####  include OpenCV  ####

LIBS += -L/usr/lib/x86_64-linux-gnu \
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc \
-lopencv_aruco \
-lopencv_ccalib \
-lopencv_calib3d \
-lopencv_features2d
