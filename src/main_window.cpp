#include "main_window.h"
#include "ui_main_window.h"
#include "ui_calibration_widget.h"

#include <QPushButton>
#include <QDockWidget>

using namespace Calibration;

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Main_Window)
{
    ui_->setupUi(this);
    this->setWindowTitle("Multi Camera Calibration Tool");
    calibration_interface_.init(&data_);
    ui_->calibration_widget->init(&data_);
    ui_->cam_params_widget->init(&data_.estimation);

    //GUI updates
    connect(&calibration_interface_, &Calibration_Interface::updateGUI, ui_->calibration_widget, &Calibration_Widget::updateGUI);
    connect(&data_, &Data::updateGUI, ui_->calibration_widget, &Calibration_Widget::updateGUI);
    connect(&data_, &Data::updateCameras, ui_->cam_params_widget, &Cam_Params_Widget::updateGUI);

    //connect logger
    connect(ui_->calibration_widget, &Calibration_Widget::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&calibration_interface_, &Calibration_Interface::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&calibration_interface_, &Calibration_Interface::logMat, ui_->logger, &Qt_Tools::Logger::logMat);

    //connect calibration pattern buttons
    connect(ui_->calibration_widget->getUI()->button_load_pattern, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::loadPattern);
    connect(ui_->calibration_widget->getUI()->button_generate_pattern, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::createPattern);
    connect(ui_->calibration_widget->getUI()->button_save_pattern, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::savePattern);

    //connect pattern detection buttons
    connect(ui_->calibration_widget->getUI()->button_detect_correspondences, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::detectPatterns);
    connect(ui_->calibration_widget->getUI()->button_load_correspondences, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::loadCorrespondences);

    //connect intrinsics buttons
    connect(ui_->calibration_widget->getUI()->button_calculate_intrinsics, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::calculateIntrinsics);
    connect(ui_->calibration_widget->getUI()->button_load_intrinsics, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::loadIntrinsics);
    connect(ui_->calibration_widget->getUI()->button_calc_pattern_pos, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::calculatePatternPositions);
    connect(ui_->calibration_widget->getUI()->button_load_pattern_pos, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::loadPatternPositions);

    //connect optimization
    connect(ui_->calibration_widget->getUI()->button_start_optimization, &QPushButton::clicked, &calibration_interface_, &Calibration_Interface::calibrate);

    //connect graphics view
    qRegisterMetaType< cv::Mat >("cv::Mat&");
    connect(&data_, &Data::updateImage, ui_->pattern_view, &Qt_Tools::Graphics_View::setCvImage);

    //connect camera parameter widget
    connect(ui_->cam_params_widget, &Cam_Params_Widget::loadCams, &calibration_interface_, &Calibration_Interface::loadCameras);
    connect(ui_->cam_params_widget, &Cam_Params_Widget::saveCams, &calibration_interface_, &Calibration_Interface::saveCameras);
}

Main_Window::~Main_Window(){
    delete ui_;
}
