#include "matrix_edit.h"

using namespace std;

Qt_Tools::Matrix_Edit::Matrix_Edit(QWidget *parent) : QWidget(parent){
    layout_ = new QGridLayout(this);
    layout_->setContentsMargins(0,0,0,0);
    this->setLayout(layout_);
}

void Qt_Tools::Matrix_Edit::init(const int& rows, const int& cols){
    if(rows <= 0 || cols <= 0){
        return;
    }
    entries = vector<vector<QLineEdit*>>(rows,vector<QLineEdit*>(cols,nullptr));
    for(int i = 0; i< rows; i++){
        for(int j = 0; j< cols; j++){
            QLineEdit* entry = new QLineEdit("0.0");
            entry->setObjectName(QString::number(i)+"_"+QString::number(j));
            layout_->addWidget(entry,i,j);
            entries[i][j] = entry;
            connect(entry, &QLineEdit::textChanged, this, &Matrix_Edit::changed);
        }
    }
    rows_ = rows;
    cols_ = cols;
}

void Qt_Tools::Matrix_Edit::set(const cv::Mat& mat){
    if(mat.rows != rows_ || mat.cols != cols_ || mat.type() != CV_64F){
        return;
    }
    this->blockSignals(true);
    for(int i = 0; i< rows_; i++){
        for(int j = 0; j< cols_; j++){
            entries[i][j]->setText(QString::number(mat.at<double>(i,j)));
        }
    }
    this->blockSignals(false);
}

void Qt_Tools::Matrix_Edit::get(cv::Mat* mat){
    *mat = cv::Mat(rows_,cols_,CV_64F);
    for(int i = 0; i< rows_; i++){
        for(int j = 0; j< cols_; j++){
            mat->at<double>(i,j) = entries[i][j]->text().toDouble();
        }
    }
}
