#include "cam_params_widget.h"
#include "ui_cam_params_widget.h"

Cam_Params_Widget::Cam_Params_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Cam_Params_Widget)
{
    ui_->setupUi(this);
    ui_->matrix_edit_K->init(3,3);
    ui_->matrix_edit_dist_k->init(1,3);
    ui_->matrix_edit_dist_p->init(1,2);
    ui_->matrix_edit_pose->init(4,4);

    connect(ui_->comboBox_idx, qOverload<int>(&QComboBox::currentIndexChanged), this, &Cam_Params_Widget::setActiveParameters);
    connect(ui_->matrix_edit_K, &Qt_Tools::Matrix_Edit::changed, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->matrix_edit_pose, &Qt_Tools::Matrix_Edit::changed, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->matrix_edit_dist_k, &Qt_Tools::Matrix_Edit::changed, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->matrix_edit_dist_p, &Qt_Tools::Matrix_Edit::changed, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->checkBox_calc_intrinsics, &QCheckBox::clicked, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->checkBox_refine_intrinsics, &QCheckBox::clicked, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->checkBox_optimize_extrinsics, &QCheckBox::clicked, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->button_set_calc_intr, &QPushButton::clicked, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->button_set_refine_intr, &QPushButton::clicked, this, &Cam_Params_Widget::updateCameraParameters);
    connect(ui_->button_set_opt_extr, &QPushButton::clicked, this, &Cam_Params_Widget::updateCameraParameters);

    connect(ui_->button_load_cams, &QPushButton::clicked, this, &Cam_Params_Widget::loadCams);
    connect(ui_->button_save_cams, &QPushButton::clicked, this, &Cam_Params_Widget::saveCams);
}

Cam_Params_Widget::~Cam_Params_Widget(){
    delete ui_;
}

bool Cam_Params_Widget::init(Calibration::Estimation* estimation){
    if(estimation == nullptr){
        return false;
    }

    estimation_ = estimation;
    return true;
}

void Cam_Params_Widget::updateGUI(){
    QString current_cam_idx = ui_->comboBox_idx->currentText();
    int idx = 0;

    this->blockSignals(true);

    ui_->comboBox_idx->clear();
    for(size_t i = 0; i < estimation_->cams.size(); i++){
        ui_->comboBox_idx->addItem(QString::number(i));
        if(current_cam_idx.compare(QString::number(i)) == 0){
            idx = i;
        }
    }
    ui_->comboBox_idx->setCurrentIndex(idx);
    ui_->groupBox->setEnabled(true);

    this->blockSignals(false);
}

void Cam_Params_Widget::setActiveParameters(int idx){

    if(idx == -1 || idx > (int) estimation_->cams.size()){
        return;
    }

    Calibration::Camera& cam = estimation_->cams[idx];

    ui_->groupBox->setEnabled(false);

    ui_->display_id->setText(QString::fromStdString(cam.id));
    ui_->display_resolution->setText(QString::number(cam.resolution_x)+" x "+QString::number(cam.resolution_y));
    ui_->matrix_edit_K->set(cam.K);
    cv::Mat dist = cam.getDistortion();
    ui_->matrix_edit_dist_p->set(dist.colRange(2,4));
    dist.at<double>(0,2) = dist.at<double>(0,4);
    ui_->matrix_edit_dist_k->set(dist.colRange(0,3));
    ui_->matrix_edit_pose->set(cam.pose);
    ui_->checkBox_calc_intrinsics->setChecked(estimation_->cams[idx].calculate_intrinsics);
    ui_->checkBox_refine_intrinsics->setChecked(estimation_->cams[idx].refine_intrinsics);
    ui_->checkBox_optimize_extrinsics->setChecked(estimation_->cams[idx].optimize_extrinsics);

    ui_->groupBox->setEnabled(true);
    ui_->comboBox_idx->setFocus();
}

void Cam_Params_Widget::updateCameraParameters(){
    int idx = ui_->comboBox_idx->currentIndex();

    QObject* sender = this->sender();
    if(sender->objectName() == "checkBox_refine_intrinsics"){
        estimation_->cams[idx].refine_intrinsics = ui_->checkBox_refine_intrinsics->isChecked();
    }
    else if(sender->objectName() == "checkBox_calc_intrinsics"){
        estimation_->cams[idx].calculate_intrinsics = ui_->checkBox_calc_intrinsics->isChecked();
    }
    else if(sender->objectName() == "checkBox_optimize_extrinsics"){
        estimation_->cams[idx].optimize_extrinsics = ui_->checkBox_optimize_extrinsics->isChecked();
    }
    else if(sender->objectName() == "matrix_edit_K"){
        ui_->matrix_edit_K->get(&estimation_->cams[idx].K);
    }
    else if(sender->objectName() == "matrix_edit_pose"){
        ui_->matrix_edit_pose->get(&estimation_->cams[idx].pose);
    }
    else if(sender->objectName() == "matrix_edit_dist_k" || sender->objectName() == "matrix_edit_dist_p"){
        cv::Mat dist_k, dist_p;
        ui_->matrix_edit_dist_k->get(&dist_k);
        ui_->matrix_edit_dist_p->get(&dist_p);
        estimation_->cams[idx].distortion.k_1 = dist_k.at<double>(0,0);
        estimation_->cams[idx].distortion.k_2 = dist_k.at<double>(0,1);
        estimation_->cams[idx].distortion.k_3 = dist_k.at<double>(0,2);
        estimation_->cams[idx].distortion.p_1 = dist_p.at<double>(0,0);
        estimation_->cams[idx].distortion.p_2 = dist_p.at<double>(0,1);
    }
    else if(sender->objectName() == "button_set_calc_intr"){
        bool calc = ui_->checkBox_calc_intrinsics->isChecked();
        for(size_t i = 0; i < estimation_->cams.size(); i++){
            estimation_->cams[i].calculate_intrinsics = calc;
        }
    }
    else if(sender->objectName() == "button_set_refine_intr"){
        bool refine = ui_->checkBox_refine_intrinsics->isChecked();
        for(size_t i = 0; i < estimation_->cams.size(); i++){
            estimation_->cams[i].refine_intrinsics = refine;
        }
    }
    else if(sender->objectName() == "button_set_opt_extr"){
        bool calc = ui_->checkBox_optimize_extrinsics->isChecked();
        for(size_t i = 0; i < estimation_->cams.size(); i++){
            estimation_->cams[i].optimize_extrinsics = calc;
        }
    }
}
