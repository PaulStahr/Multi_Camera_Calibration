#ifndef MATRIX_EDIT_H
#define MATRIX_EDIT_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QGridLayout>
#include <QLineEdit>
#include <QWidget>

namespace Qt_Tools{

class Matrix_Edit : public QWidget
{
    Q_OBJECT
public:
    explicit Matrix_Edit(QWidget *parent = nullptr);

public slots:
    void init(const int& rows, const int& cols);

    void set(const cv::Mat& mat);
    void get(cv::Mat* mat);

private:
    QGridLayout* layout_;
    std::vector<std::vector<QLineEdit*>> entries;
    int rows_ = 0;
    int cols_ = 0;

signals:
    void changed();
};
}
#endif // MATRIX_EDIT_H
