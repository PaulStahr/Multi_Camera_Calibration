#include "optimization.h"

#include <QThread>
#include <QTime>

using namespace std;
using namespace cv;
using namespace Calibration;

Optimization::Optimization(QObject *parent) : QObject(parent){

}

bool Optimization::intrinsicsEstimation(const Detection& detection, Estimation* estimation, const int& initial_intrinsics_mode){
    if(estimation->cams.size() != (size_t) detection.num_cams){
        WARN("Not all cameras have detected patterns available. Thus not all intrinsics can be calculated.");
    }

    //initialize estimation tables
    estimation->rotations = vector_3D<Mat>(detection.num_cams,vector_2D<Mat>(detection.num_pattern_positions,vector<Mat>(detection.num_rig_positions,Mat(0,0,CV_64F))));
    estimation->translations = vector_3D<Mat>(detection.num_cams,vector_2D<Mat>(detection.num_pattern_positions,vector<Mat>(detection.num_rig_positions,Mat(0,0,CV_64F))));
    estimation->transforms = vector_3D<Mat>(detection.num_cams,vector_2D<Mat>(detection.num_pattern_positions,vector<Mat>(detection.num_rig_positions,Mat(0,0,CV_64F))));

    //start intrinsics estimations in different threads
    vector<Optimization*> optimizations;
    vector<QThread*> threads;
    vector<double> rmss(detection.num_cams, 0.0);

    qRegisterMetaType< Detection >("Detection");
    qRegisterMetaType< Camera >("Camera");
    Safe_Counter counter;

    for (int cam_idx = 0; cam_idx < detection.num_cams; cam_idx++){
        Optimization* new_opt = new Optimization();
        optimizations.push_back(new_opt);
        QThread* new_thread = new QThread();
        optimizations.back()->moveToThread(new_thread);
        threads.push_back(new_thread);
        connect(this, &Optimization::startIntrinsicsThread, new_opt, &Optimization::singleIntrinsicsEstimation);
        new_thread->start();
        int cam_idx_est = estimation->cam_id_to_idx_map[detection.detected_cam_ids[cam_idx]];
        emit startIntrinsicsThread(detection, estimation, (initial_intrinsics_mode > 0), cam_idx_est, cam_idx, &(estimation->cams[cam_idx_est]), &(rmss[cam_idx]), &counter);
        disconnect(this, &Optimization::startIntrinsicsThread, new_opt, &Optimization::singleIntrinsicsEstimation);
    }

    //wait for all threads to finish
    while(counter.get()!=detection.num_cams){
        QThread::msleep(250);
    }

    //delete threads and log calculated data
    for (int cam_idx = 0; cam_idx < detection.num_cams; cam_idx++){

        threads[cam_idx]->quit();
        threads[cam_idx]->deleteLater();
        optimizations[cam_idx]->deleteLater();

        int cam_idx_est = estimation->cam_id_to_idx_map[detection.detected_cam_ids[cam_idx]];
        STATUS("Camera "+QString::number(cam_idx_est)+" rms: "+QString::number(rmss[cam_idx]));
        LOGMATRIX("K matrix:",estimation->cams[cam_idx_est].K);
        LOGMATRIX("Distortion:", estimation->cams[cam_idx_est].getDistortion());
    }

    estimation->updateCameras();

    return true;
}

void Optimization::singleIntrinsicsEstimation(const Detection& detection, Estimation* estimation, const bool& use_initial_intrinsics, const int& cam_idx_est, const int& cam_idx_det, Camera* cam, double* rms, Safe_Counter* counter){
    //convert detected Feat_Points to opencv compatible format
    vector<vector<Point3f>> object_points;
    vector<vector<Point2f>> image_points;
    bool correspondences_available = false;
    for(int pattern_idx = 0; pattern_idx < detection.num_pattern_positions; pattern_idx++){
        for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
            if(detection.correspondences[cam_idx_det][pattern_idx][rig_idx].size() != 0){
                vector<Point3f> object_points_for_image;
                vector<Point2f> image_points_for_image;

                for(size_t point_idx = 0; point_idx < detection.correspondences[cam_idx_det][pattern_idx][rig_idx].size(); point_idx++){
                    const Feature_Point& p = detection.correspondences[cam_idx_det][pattern_idx][rig_idx][point_idx];
                    object_points_for_image.push_back((Point3f)p.corr);
                    image_points_for_image.push_back((Point2f)p.p);
                }

                object_points.push_back(object_points_for_image);
                image_points.push_back(image_points_for_image);
                correspondences_available = true;
            }
        }
    }

    if(!correspondences_available){
        ERROR("No correspondences available for intrinsic estimation of camera "+QString::fromStdString(cam->id));
        counter->up();
        return;
    }

    //prepare arguments for opencv intrinsic calibration
    Mat K;
    if(use_initial_intrinsics){
        K = cam->K.clone();
    }
    else{
        double K_data[9] = { 1.0, 0.0, (double)cam->resolution_x/2.0, 0.0, 1.0, (double)cam->resolution_y/2.0, 0.0, 0.0, 1.0 };
        K = Mat(3,3,CV_64F,K_data);
    }
    Mat distortion_coeffs;

    //intrinsic calibration
    if(estimation->cams[cam_idx_est].calculate_intrinsics){
        vector<Mat> rotations;
        vector<Mat> translations;
        Size2i size(cam->resolution_x,cam->resolution_y);
        if(use_initial_intrinsics){
            *rms = cv::calibrateCamera(object_points, image_points, size, K, distortion_coeffs, rotations, translations, CALIB_USE_INTRINSIC_GUESS);
        }
        else{
            *rms = cv::calibrateCamera(object_points, image_points, size, K, distortion_coeffs, rotations, translations);
        }

        //save camera matrix and estimated rotations/translations
        K.convertTo(cam->K, CV_64F);
        cv::Mat distortion_mat;
        distortion_coeffs.convertTo(distortion_mat, CV_64F);
        cam->setDistortion(distortion_mat);

        size_t tf_counter = 0;
        for(int pattern_idx = 0; pattern_idx < detection.num_pattern_positions; pattern_idx++){
            for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
                if(detection.correspondences[cam_idx_det][pattern_idx][rig_idx].size() != 0 && tf_counter < rotations.size()){
                    rotations[tf_counter].convertTo(estimation->rotations[cam_idx_est][pattern_idx][rig_idx],CV_64F);
                    translations[tf_counter].convertTo(estimation->translations[cam_idx_est][pattern_idx][rig_idx],CV_64F);
                    tf_counter++;
                }
            }
        }
    }
    else{
        K = cam->K.clone();
        distortion_coeffs = cam->getDistortion();
        Mat rotation, translation;
        for(int pattern_idx = 0; pattern_idx < detection.num_pattern_positions; pattern_idx++){
            for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
                if(detection.correspondences[cam_idx_det][pattern_idx][rig_idx].size() != 0){
                    *rms = cv::solvePnP(detection.object_points[cam_idx_det][pattern_idx][rig_idx], detection.image_points[cam_idx_det][pattern_idx][rig_idx], K, distortion_coeffs, rotation, translation);
                    rotation.convertTo(estimation->rotations[cam_idx_est][pattern_idx][rig_idx],CV_64F);
                    translation.convertTo(estimation->translations[cam_idx_est][pattern_idx][rig_idx],CV_64F);
                }
            }
        }
    }

    counter->up();
    return;
}

bool Optimization::optimizeExtrinsics(const Detection& detection, Estimation* estimation, const LM_Params& params, const Optimization_Params& opt_params){
    //build graph
    STATUS("Building graph..");
    Graph graph;

    if(!buildGraph(detection, estimation, opt_params.initial_extrinsics_mode, &graph)){
        ERROR("Graph construction failed.");
        return false;
    }

    STATUS("Initial camera poses:");
    for(size_t i = 0; i < estimation->cams.size(); i++){
        LOGMATRIX("Camera "+QString::number(i),estimation->cams[i].pose);
    }

    //optimize extrinsics
    STATUS("Start extrinsics optimization..");
    double error;
    if(!optimizeExtrinsicsLM(detection, estimation, &graph, &error, params, opt_params)){
        ERROR("Optimization failed.");
        return false;
    }

    STATUS("Final camera poses:");
    for(size_t i = 0; i < estimation->cams.size(); i++){
        LOGMATRIX("Camera "+QString::number(i),estimation->cams[i].pose);
    }
    if(opt_params.refine_rig){
        STATUS("Refined rig axes:");
        LOGMATRIX("x axis",estimation->x_axis);
        LOGMATRIX("y_axis",estimation->y_axis);
    }
    STATUS("Optimization finished with an error of "+QString::number(error));
    return true;
}

bool Optimization::buildGraph(const Detection& detection, Estimation* estimation, const int& initial_extrinsics_mode, Graph* graph){

    if(graph ==  nullptr){
        ERROR("Invalid parameters for graph building.");
        return false;
    }
    if(detection.num_cams == 0 || detection.num_pattern_positions == 0 || detection.num_rig_positions == 0){
        ERROR("In order to build the optimization graph, the camera estimations have to be initialized.");
        return false;
    }

    graph->cam_vertices.clear();
    graph->pattern_vertices.clear();
    graph->edges.clear();

    //create vertices
    vector<int> idx_for_cam_vert;
    vector<int> idx_for_pattern_vert;
    for(int i = 0; i<detection.num_cams; i++){
        graph->cam_vertices.push_back(Camera_Vertex());
        graph->cam_vertices.back().camera_idx = i;
        idx_for_cam_vert.push_back(i);
        if(i == 0){
            graph->cam_vertices.back().pose_calculated = true;
        }
    }
    for(int i = 0; i<detection.num_pattern_positions; i++){
        graph->pattern_vertices.push_back(Pattern_Vertex());
        graph->pattern_vertices.back().pattern_position = i;
        idx_for_pattern_vert.push_back(i);
    }

    //create edges and calculate transforms
    for(int cam_idx = 0; cam_idx < detection.num_cams; cam_idx++){
        for(int pattern_idx = 0; pattern_idx < detection.num_pattern_positions; pattern_idx++){
            for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){

                Mat rotation = estimation->rotations[cam_idx][pattern_idx][rig_idx];
                Mat translation = estimation->translations[cam_idx][pattern_idx][rig_idx];

                if(rotation.rows == 0 || translation.rows == 0){
                    estimation->transforms[cam_idx][pattern_idx][rig_idx] = cv::Mat(0,0,CV_64F).clone();
                    continue;
                }

                Mat transform = Mat::eye(4, 4, CV_64F);
                Mat R, T;
                Rodrigues(rotation, R);
                T = translation.reshape(1, 3);
                R.copyTo(transform.rowRange(0, 3).colRange(0, 3));
                T.copyTo(transform.rowRange(0, 3).col(3));

                transform.copyTo(estimation->transforms[cam_idx][pattern_idx][rig_idx]);

                graph->edges.push_back(Edge(cam_idx, pattern_idx, rig_idx, transform));
            }
        }
    }

    //precalculate rig transforms
    vector<Mat> rig_transform;
    for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
        Mat transform = Mat::eye(4, 4, CV_64F);
        Mat translation = detection.rig_positions[rig_idx].first*estimation->x_axis + detection.rig_positions[rig_idx].second*estimation->y_axis;
        translation.copyTo(transform.rowRange(0,3).col(3));
        rig_transform.push_back(transform);
    }

    //traverse graph in order to calculate initial poses for all vertices
    vector<bool> cams_visited(graph->cam_vertices.size(),false);
    vector<bool> patterns_visited(graph->pattern_vertices.size(),false);

    set<int> queue_cam = {0};
    set<int> queue_pattern = {};

    while(queue_cam.size() != 0 || queue_pattern.size() != 0){

        bool is_cam_vertex = true;
        int vert_idx;

        //check if current vertex is cam certex or pattern vertex
        if(queue_cam.size() != 0){
            vert_idx = *queue_cam.begin();
            queue_cam.erase(queue_cam.begin());
        }
        else{
            vert_idx = *queue_pattern.begin();
            queue_pattern.erase(queue_pattern.begin());
            is_cam_vertex = false;
        }

        //check if vertex has already been visited
        if(is_cam_vertex && cams_visited[vert_idx]){
            continue;
        }
        else if(!is_cam_vertex && patterns_visited[vert_idx]){
            continue;
        }

        //add children to queue if unvisited and calculate initial poses
        if(is_cam_vertex){
            for(int pattern_idx = 0; pattern_idx < detection.num_pattern_positions; pattern_idx++){
                for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
                    if(estimation->transforms[vert_idx][pattern_idx][rig_idx].rows != 0){
                        if(!patterns_visited[pattern_idx] && !graph->pattern_vertices.at(pattern_idx).pose_calculated){
                            //set parent to child
                            graph->pattern_vertices.at(pattern_idx).parent_camera_idx = vert_idx;
                            //calculate pose for child
                            graph->pattern_vertices.at(pattern_idx).pose = graph->cam_vertices.at(vert_idx).pose.inv() * rig_transform[rig_idx] * estimation->transforms[vert_idx][pattern_idx][rig_idx];
                            graph->pattern_vertices.at(pattern_idx).pose_calculated = true;
                            //add pattern to queue
                            queue_pattern.insert(pattern_idx);
                        }
                        //add (child index,rig position) pair to the current vertex's child list
                        graph->cam_vertices.at(vert_idx).child_pattern_rig_idx.push_back(pairi(pattern_idx,rig_idx));
                    }
                }
            }
        }
        else{
            for(int cam_idx = 0; cam_idx < detection.num_cams; cam_idx++){
                for(int rig_idx = 0; rig_idx < detection.num_rig_positions; rig_idx++){
                    if(estimation->transforms[cam_idx][vert_idx][rig_idx].rows != 0){
                        if(!cams_visited[cam_idx] && !graph->cam_vertices.at(cam_idx).pose_calculated){
                            //set parent to child
                            graph->cam_vertices.at(cam_idx).parent_pattern_idx = vert_idx;
                            //calculate pose for child
                            graph->cam_vertices.at(cam_idx).pose = rig_transform[rig_idx] * estimation->transforms[cam_idx][vert_idx][rig_idx] * graph->pattern_vertices.at(vert_idx).pose.inv();
                            graph->cam_vertices.at(cam_idx).pose_calculated = true;
                            //add pattern to queue
                            queue_cam.insert(cam_idx);
                        }
                        //add (child index,rig position) pair to the current vertex's child list
                        graph->pattern_vertices.at(vert_idx).child_camera_rig_idx.push_back(pairi(cam_idx,rig_idx));
                    }
                }
            }
        }
    }

    //delete vertices without or too few connections
    bool changed = true;
    while(changed){
        changed = false;

        bool is_cam_vertex = true;
        int vert_idx = -1;

        //search for vertex to delete
        for(size_t i = 0; i < graph->cam_vertices.size(); i++){
            if(graph->cam_vertices.at(i).child_pattern_rig_idx.size() == 0){//camera vertex without connections
                vert_idx = i;
                break;
            }
        }
        if(vert_idx == -1){
            for(size_t i = 0; i < graph->pattern_vertices.size(); i++){
                if(graph->pattern_vertices.at(i).child_camera_rig_idx.size() <= 1){//pattern vertex with no or only one connection
                    vert_idx = i;
                    is_cam_vertex = false;
                    break;
                }
            }
        }
        if(vert_idx == -1){
            continue;
        }

        //delete vertex and all corresponding entries in edges and other vertices
        changed = true;
        if(is_cam_vertex){
            int vert_pos = graph->cam_vertices[vert_idx].camera_idx;
            WARN("Camera "+QString::fromStdString(estimation->cams[graph->cam_vertices[idx_for_cam_vert[vert_pos]].camera_idx].id)+" does not have sufficient correspondences with other cameras to be optimized.");
            QThread::sleep(2);
            graph->cam_vertices.erase(graph->cam_vertices.begin()+idx_for_cam_vert[vert_pos]); //delete vertex
            idx_for_cam_vert[vert_pos] = -1; //set idx
            for(size_t i = vert_pos+1; i < idx_for_cam_vert.size(); i++){
                idx_for_cam_vert[i]--;
            }

            //delete corresponding edges
            for(size_t i = 0; i < graph->edges.size(); i++){
                if(graph->edges.at(i).cam_vertex == vert_pos){
                    graph->edges.erase(graph->edges.begin()+i);
                    i--;
                }
            }
            //delete from child lists of pattern vertices
            for(size_t i = 0; i < graph->pattern_vertices.size(); i++){
                for(size_t j = 0; j < graph->pattern_vertices.at(i).child_camera_rig_idx.size(); j++){
                    if(graph->pattern_vertices.at(i).child_camera_rig_idx[j].first == vert_pos){
                        graph->pattern_vertices.at(i).child_camera_rig_idx.erase(graph->pattern_vertices.at(i).child_camera_rig_idx.begin()+j);
                        j--;
                    }
                }
            }
        }
        else{
            int vert_pos = graph->pattern_vertices[vert_idx].pattern_position;
            graph->pattern_vertices.erase(graph->pattern_vertices.begin()+idx_for_pattern_vert[vert_pos]); //delete vertex
            idx_for_pattern_vert[vert_pos] = -1; //set idx
            for(size_t i = vert_pos+1; i < idx_for_pattern_vert.size(); i++){
                idx_for_pattern_vert[i]--;
            }

            //delete corresponding edges
            for(size_t i = 0; i < graph->edges.size(); i++){
                if(graph->edges.at(i).pattern_vertex == vert_pos){
                    graph->edges.erase(graph->edges.begin()+i);
                    i--;
                }
            }
            //delete from child lists of cam vertices
            for(size_t i = 0; i < graph->cam_vertices.size(); i++){
                for(size_t j = 0; j < graph->cam_vertices.at(i).child_pattern_rig_idx.size(); j++){
                    if(graph->cam_vertices.at(i).child_pattern_rig_idx[j].first == vert_pos){
                        graph->cam_vertices.at(i).child_pattern_rig_idx.erase(graph->cam_vertices.at(i).child_pattern_rig_idx.begin()+j);
                        j--;
                    }
                }
            }
        }
    }

    //reset vertex indices in edges to match the vertex lists after the deletion process
    for(Edge& edge : graph->edges){
        edge.cam_vertex = idx_for_cam_vert[edge.cam_vertex];
        edge.pattern_vertex = idx_for_pattern_vert[edge.pattern_vertex];
    }

    //set idx lists
    estimation->refine_idx = vector<int>(estimation->cams.size(), -1);
    estimation->calculation_idx = vector<int>(estimation->cams.size(), -1);
    int refine_counter = 0;
    int calc_counter = 0;
    for(const Camera_Vertex& cam : graph->cam_vertices){
        if(estimation->cams[cam.camera_idx].refine_intrinsics){
            estimation->refine_idx[cam.camera_idx] = refine_counter;
            refine_counter++;
        }
        if(estimation->cams[cam.camera_idx].optimize_extrinsics){
            estimation->calculation_idx[cam.camera_idx] = calc_counter;
            calc_counter++;
        }
    }

    if(initial_extrinsics_mode == INIT_EXTRINSICS_FROM_GUI){
        for(Camera_Vertex& cam : graph->cam_vertices){
            estimation->cams[cam.camera_idx].pose.copyTo(cam.pose);
        }
    }

    estimation->updateCameras();

    return true;
}

bool Optimization::optimizeExtrinsicsLM(const Detection& detection, Estimation* estimation, Graph* graph, double* error, const Calibration::LM_Params& params, const Optimization_Params& opt_params){
    //prepare initial solution for LM optimization
    int num_cams_to_optimize = 0;
    estimation->calc_extrinsics_idx_list.clear();
    for(unsigned int i = 1; i < graph->cam_vertices.size(); i++){
        const Camera_Vertex& cam_vertex = graph->cam_vertices[i];
        if(estimation->cams[cam_vertex.camera_idx].optimize_extrinsics){
            estimation->calc_extrinsics_idx_list.push_back(i);
            num_cams_to_optimize++;
        }
    }
    std::cerr << num_cams_to_optimize;

    int num_vertices = int(estimation->calc_extrinsics_idx_list.size()) + int(graph->pattern_vertices.size());
    Mat extrinsics(1, num_vertices*6, CV_64F);
    int offset = 0;
    for (size_t i = 0; i < estimation->calc_extrinsics_idx_list.size(); i++){
        //get rodrigues rotation and translation from pose saved in vertex
        Mat rotation, translation;
        cv::Rodrigues(graph->cam_vertices[estimation->calc_extrinsics_idx_list[i]].pose.rowRange(0,3).colRange(0, 3), rotation);
        graph->cam_vertices[estimation->calc_extrinsics_idx_list[i]].pose.rowRange(0,3).col(3).copyTo(translation);
        rotation.reshape(1, 1).copyTo(extrinsics.colRange(offset, offset + 3));
        translation.reshape(1, 1).copyTo(extrinsics.colRange(offset+3, offset +6));
        offset += 6;
    }
    for (size_t i = 0; i < graph->pattern_vertices.size(); i++){
        //get rodrigues rotation and translation from pose saved in vertex
        Mat rotation, translation;
        cv::Rodrigues(graph->pattern_vertices.at(i).pose.rowRange(0,3).colRange(0, 3), rotation);
        graph->pattern_vertices.at(i).pose.rowRange(0,3).col(3).copyTo(translation);
        rotation.reshape(1, 1).copyTo(extrinsics.colRange(offset, offset + 3));
        translation.reshape(1, 1).copyTo(extrinsics.colRange(offset+3, offset +6));

        offset += 6;
    }

    //prepare initial intrinsics vector for refinement
    Mat intrinsics;
    int intrinsics_size = 0;
    if(opt_params.refine_intrinsics_mode == REFINE_INTRINSICS_GUI){
        for (const Camera& cam : estimation->cams){
            if(cam.refine_intrinsics){
                intrinsics_size++;
            }
        }
    }
    else if(opt_params.refine_intrinsics_mode == REFINE_INTRINSICS_ALL){
        intrinsics_size = (int)(graph->cam_vertices.size());
    }

    if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
        intrinsics = cv::Mat(1, intrinsics_size*9, CV_64F);
        offset = 0;
        for (size_t i = 0; i < graph->cam_vertices.size(); i++){
            if(estimation->cams[i].refine_intrinsics || opt_params.refine_intrinsics_mode == REFINE_INTRINSICS_ALL){
                Mat fx_fy_cx_cy(1,4, CV_64F);
                fx_fy_cx_cy.at<double>(0,0) = estimation->cams[graph->cam_vertices[i].camera_idx].K.at<double>(0,0);
                fx_fy_cx_cy.at<double>(0,1) = estimation->cams[graph->cam_vertices[i].camera_idx].K.at<double>(1,1);
                fx_fy_cx_cy.at<double>(0,2) = estimation->cams[graph->cam_vertices[i].camera_idx].K.at<double>(0,2);
                fx_fy_cx_cy.at<double>(0,3) = estimation->cams[graph->cam_vertices[i].camera_idx].K.at<double>(1,2);
                Mat distortion = estimation->cams[graph->cam_vertices[i].camera_idx].getDistortion();

                fx_fy_cx_cy.copyTo(intrinsics.colRange(offset,offset+4));
                distortion.copyTo(intrinsics.colRange(offset+4, offset+9));

                offset += 9;
            }
        }
    }
    else{
        intrinsics = cv::Mat(0,0,CV_64F);
    }

    //prepare initial rig vector for refinement
    Mat rig;
    if(opt_params.refine_rig){
        rig = cv::Mat(1, 6, CV_64F);
        rig.colRange(0,3) = estimation->x_axis.t();
        rig.colRange(3,6) = estimation->y_axis.t();
    }
    else{
        rig = cv::Mat(0,0,CV_64F);
    }

    int num_extrinsics = extrinsics.total();
    int num_intrinsics = intrinsics.total();
    int num_rig = rig.total();

    //Levenberg Marquardt optimization
    double change = 1.0;
    double lambda = 1e-5; //initial step size
    *error = 1e+100;
    double current_error;
    Mat J, J_last, Error, Error_last, DtD, JtJ_step, step, JtJ;
    bool last_failed = false;

    for(int iter = 0; ; iter++){
        //check termination criteria
        if ((params.term_criterium == LM_TERM_MAX_ITERATIONS && iter >= params.max_iterations) ||
                (params.term_criterium == LM_TERM_CHANGE_THRESHOLD && change <= params.change_threshold) ||
                (params.term_criterium == LM_TERM_COMBINED && (change <= params.change_threshold || iter >= params.max_iterations))){
            break;
        }

        //if the error increased in the last iteration, reset the jacobian etc. (but not the stepsize!)
        if(last_failed){
            last_failed = false;
            STATUS("Iteration "+QString::number(iter-1)+" failed. Resetting.");

            J = J_last.clone();
            Error = Error_last.clone();
            JtJ =  J.t() * J;
            DtD = cv::Mat::eye(J.cols,J.cols,CV_64F).mul(JtJ);
            JtJ_step = JtJ + lambda * DtD;
            solve(JtJ_step,J.t() * Error,step);
            step = step.reshape(1,1);
            extrinsics = extrinsics + step.colRange(0,num_extrinsics);
            if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
                intrinsics = intrinsics + step.colRange(num_extrinsics,num_extrinsics+num_intrinsics);
            }
            if(opt_params.refine_rig){
                rig = rig + step.colRange(num_extrinsics+num_intrinsics,num_extrinsics+num_intrinsics+num_rig);
            }
            iter--;
            continue;
        }

        //calculate jacobian
        calculateJacobian(detection, *estimation, extrinsics, intrinsics, opt_params, rig, *graph, &J, &Error);

        current_error = 0.0;
        for(int point_idx = 0; point_idx < Error.rows/2; point_idx++){
            current_error += pow(Error.at<double>(2*point_idx,0),2.0)+pow(Error.at<double>(2*point_idx+1,0),2.0);
        }
        current_error = sqrt(current_error/(double)(Error.rows/2));

        STATUS("Iteration "+QString::number(iter)+" Error: "+QString::number(current_error)+" Stepsize: "+QString::number(lambda));

        //calculate change
        change = *error - current_error;
        //adjust stepsize accordingly
        if(current_error > *error){
            if(iter == 0){
                ERROR("Initial LM error for extrinsic calibration too high.");
                return false;
            }

            step = step.reshape(1,1);
            extrinsics = extrinsics - step.colRange(0,num_extrinsics);
            if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
                intrinsics = intrinsics - step.colRange(num_extrinsics,num_extrinsics+num_intrinsics);
            }
            if(opt_params.refine_rig){
                rig = rig - step.colRange(num_extrinsics+num_intrinsics,num_extrinsics+num_intrinsics+num_rig);
            }

            lambda *= 10.0;
            last_failed = true;
            change = 1.0;
            continue;
        }
        else{
            QTime time;
            *error = current_error;
            time.start();
            JtJ = J.t()*J;
            int elapsed = time.elapsed();
            STATUS("Time elapsed: "+QString::number(elapsed)+" ms");

            DtD = cv::Mat::eye(J.cols,J.cols,CV_64F).mul(JtJ);
            JtJ_step = JtJ + lambda * DtD;
            solve(JtJ_step,J.t() * Error,step);
            step = step.reshape(1,1);
            extrinsics = extrinsics + step.colRange(0,num_extrinsics);
            if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
                intrinsics = intrinsics + step.colRange(num_extrinsics,num_extrinsics+num_intrinsics);
            }
            if(opt_params.refine_rig){
                rig = rig + step.colRange(num_extrinsics+num_intrinsics,num_extrinsics+num_intrinsics+num_rig);
            }

            J_last = J.clone();
            Error_last = Error.clone();

            lambda /= 10.0;
        }
    }

    //calculate final rotations and translations from optimized poses
    step = step.reshape(1,1);
    extrinsics = extrinsics - step.colRange(0,num_extrinsics);
    if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
        intrinsics = intrinsics - step.colRange(num_extrinsics,num_extrinsics+num_intrinsics);
    }
    if(opt_params.refine_rig){
        rig = rig - step.colRange(num_extrinsics+num_intrinsics,num_extrinsics+num_intrinsics+num_rig);
    }

    //save camera pose results and intrinsics to estimated cameras
    vector<Vec3d> rotations, translations;
    for (int i = 0; i < extrinsics.cols; i+=6){
        rotations.push_back(Vec3d(extrinsics.colRange(i, i + 3)));
        translations.push_back(Vec3d(extrinsics.colRange(i + 3, i + 6)));
    }
    vector<Mat> fx_fy_cx_cy, distortions;
    if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0){
        for (int i = 0; i < intrinsics.cols; i+=9){
            fx_fy_cx_cy.push_back(Mat(intrinsics.colRange(i, i + 4)));
            distortions.push_back(Mat(intrinsics.colRange(i + 4, i + 9)));
        }
    }

    int intrinsics_counter = 0;
    int extrinsics_counter = 0;
    for (size_t i = 0; i < graph->cam_vertices.size(); i++){
        if(i!=0 && estimation->cams[graph->cam_vertices[i].camera_idx].optimize_extrinsics){
            Mat pose = Mat::eye(4,4,CV_64F);
            Mat R;
            Rodrigues(rotations[extrinsics_counter], R);
            R.copyTo(pose.rowRange(0,3).colRange(0,3));
            Mat(translations[extrinsics_counter]).reshape(1, 3).copyTo(pose.rowRange(0, 3).col(3));
            pose.copyTo(estimation->cams[graph->cam_vertices[i].camera_idx].pose);
            extrinsics_counter++;
        }

        if(opt_params.refine_intrinsics_mode != REFINE_INTRINSICS_NONE && intrinsics_size > 0 && estimation->cams[i].refine_intrinsics){
            Mat dist = distortions[intrinsics_counter].reshape(1,1);
            estimation->cams[graph->cam_vertices[i].camera_idx].setDistortion(dist);
            Mat K = cv::Mat::eye(3,3,CV_64F);
            K.at<double>(0,0) = fx_fy_cx_cy[intrinsics_counter].at<double>(0,0);
            K.at<double>(1,1) = fx_fy_cx_cy[intrinsics_counter].at<double>(0,1);
            K.at<double>(0,2) = fx_fy_cx_cy[intrinsics_counter].at<double>(0,2);
            K.at<double>(1,2) = fx_fy_cx_cy[intrinsics_counter].at<double>(0,3);
            K.copyTo(estimation->cams[graph->cam_vertices[i].camera_idx].K);
            intrinsics_counter++;
        }
    }

    for(size_t i = 0; i < graph->pattern_vertices.size(); i++){
        cv::Mat rot;
        cv::Rodrigues(rotations[i+graph->cam_vertices.size()-1],rot);
        graph->pattern_vertices[i].pose = cv::Mat::eye(4,4,CV_64F);
        rot.copyTo(graph->pattern_vertices[i].pose.colRange(0,3).rowRange(0,3));
        Mat(translations[i+graph->cam_vertices.size()-1]).reshape(1,3).copyTo(graph->pattern_vertices[i].pose.rowRange(0,3).col(3));
    }

    //save pattern transformations
    for (size_t i = 0; i < graph->edges.size(); i++){
        Edge& edge = graph->edges[i];
        int cam_idx = graph->cam_vertices[edge.cam_vertex].camera_idx;
        int pattern_idx = graph->pattern_vertices[edge.pattern_vertex].pattern_position;

        Mat transform = estimation->cams[cam_idx].pose*graph->pattern_vertices[edge.pattern_vertex].pose;

        cv::Mat rod;
        cv::Rodrigues(transform.colRange(0,3).rowRange(0,3),rod);
        cv::Mat t = transform.rowRange(0,3).col(3);

        estimation->rotations[cam_idx][pattern_idx][edge.rig_position] = rod.clone();
        estimation->translations[cam_idx][pattern_idx][edge.rig_position] = t.clone();
    }

    //save rig parameters
    if(opt_params.refine_rig){
        rig = rig.reshape(1,6);
        rig.rowRange(0,3).copyTo(estimation->x_axis);
        rig.rowRange(3,6).copyTo(estimation->y_axis);
    }

    //calculate per camera error
    vector<double> error_per_cam = vector<double>(graph->cam_vertices.size(),0.0);
    vector<int> entries_per_cam = vector<int>(graph->cam_vertices.size(),0);
    int current_offset = 0;
    for(const Edge& edge : graph->edges){
        int cam_idx = graph->cam_vertices[edge.cam_vertex].camera_idx;
        int pattern_idx = graph->pattern_vertices[edge.pattern_vertex].pattern_position;
        int rig_idx = edge.rig_position;
        int interval = int(detection.correspondences[cam_idx][pattern_idx][rig_idx].size());

        for(int point_idx = 0; point_idx < interval; point_idx++){
            error_per_cam[edge.cam_vertex] += pow(Error.at<double>(current_offset+2*point_idx,0),2.0)+pow(Error.at<double>(current_offset+2*point_idx+1,0),2.0);
        }
        entries_per_cam[edge.cam_vertex] += interval;
        current_offset += 2*interval;
    }
    for(unsigned int i = 0 ; i < error_per_cam.size(); i++){
        error_per_cam[i] = sqrt(error_per_cam[i] / double(entries_per_cam[i]));
        STATUS("Error for cam "+QString::fromStdString(estimation->cams[graph->cam_vertices[i].camera_idx].id)+" : "+QString::number(error_per_cam[i]));
    }

    estimation->updateCameras();

    return true;
}

bool Optimization::calculateJacobian(const Detection& detection, const Estimation& estimation, const Mat& extrinsics, const Mat& intrinsics, const Optimization_Params& opt_params, const Mat& rig, const Graph& graph, Mat* jacobian, Mat* error){
    bool refine_rig = !(rig.rows == 0);
    int num_params = (int)(extrinsics.total() + intrinsics.total() + rig.total());
    int num_edges = (int)graph.edges.size();

    //calculate start indices for every edge
    vector<int> start_idx(num_edges + 1, 0);
    for (int edge_idx = 0; edge_idx < num_edges; edge_idx++){
        int cam_idx = graph.cam_vertices[graph.edges[edge_idx].cam_vertex].camera_idx;
        int pattern_idx = graph.pattern_vertices[graph.edges[edge_idx].pattern_vertex].pattern_position;
        int rig_idx = graph.edges[edge_idx].rig_position;

        start_idx[edge_idx + 1] = start_idx[edge_idx] + 2* (int)detection.correspondences[cam_idx][pattern_idx][rig_idx].size();
    }

    *jacobian = Mat::zeros(start_idx[num_edges], num_params, CV_64F);
    *error = Mat::zeros(start_idx[num_edges], 1, CV_64F);

    for(int edge_idx = 0; edge_idx < num_edges; edge_idx++){
        const Edge& edge = graph.edges[edge_idx];

        int cam_idx = graph.cam_vertices[edge.cam_vertex].camera_idx;
        int pattern_idx = graph.pattern_vertices[edge.pattern_vertex].pattern_position;
        int rig_idx = edge.rig_position;

        Mat object_points = detection.object_points[cam_idx][pattern_idx][rig_idx];
        Mat image_points = detection.image_points[cam_idx][pattern_idx][rig_idx];

        //check if current cameras extrinsics should be optimized
        bool optimize_extrinsics = false;
        int optimization_index = 0;
        if(opt_params.calculate_extrinsics_mode == CALC_EXTRINSICS_ALL){
            optimize_extrinsics = true;
            optimization_index = edge.cam_vertex;
        }
        else{
            optimize_extrinsics = estimation.cams[cam_idx].optimize_extrinsics;
            optimization_index = estimation.calculation_idx[cam_idx];
        }

        //get rotations and translations of vertices, i.e. cam+pattern
        int offset = 6 * int(estimation.calc_extrinsics_idx_list.size()); //start idx of pattern parameters in extrinsics vector
        Mat Rod_pattern, T_pattern, Rod_cam, T_cam;
        Rod_pattern = extrinsics.colRange(offset+edge.pattern_vertex*6, offset+edge.pattern_vertex*6 + 3);
        Rod_pattern = Rod_pattern.reshape(1,3);
        T_pattern = extrinsics.colRange(offset+edge.pattern_vertex*6 + 3, offset+edge.pattern_vertex*6 + 6);
        T_pattern = T_pattern.reshape(1,3);

        if (edge.cam_vertex > 0 && optimize_extrinsics){
            Rod_cam = extrinsics.colRange((optimization_index-1)*6, (optimization_index-1)*6 + 3);
            Rod_cam = Rod_cam.reshape(1,3);
            T_cam = extrinsics.colRange((optimization_index-1)*6 + 3, (optimization_index-1)*6 + 6);
            T_cam = T_cam.reshape(1,3);
        }
        else{
            cv::Rodrigues(graph.cam_vertices[edge.cam_vertex].pose.colRange(0,3).rowRange(0,3), Rod_cam);
            graph.cam_vertices[edge.cam_vertex].pose.col(3).rowRange(0,3).copyTo(T_cam);
        }


        bool refine_intrinsics = false;
        int refine_idx = 0;
        Mat K = cv::Mat::eye(3,3,CV_64F);
        Distortion_Parameters d;
        if(opt_params.refine_intrinsics_mode == REFINE_INTRINSICS_ALL){
            refine_idx = edge.cam_vertex;
            refine_intrinsics = true;

            Mat intrinsic_cam = intrinsics.colRange(9*edge.cam_vertex, 9*edge.cam_vertex+9);
            K.at<double>(0,0) = intrinsic_cam.at<double>(0,0);
            K.at<double>(1,1) = intrinsic_cam.at<double>(0,1);
            K.at<double>(0,2) = intrinsic_cam.at<double>(0,2);
            K.at<double>(1,2) = intrinsic_cam.at<double>(0,3);
            d.k_1 = intrinsic_cam.at<double>(0,4);
            d.k_2 = intrinsic_cam.at<double>(0,5);
            d.p_1 = intrinsic_cam.at<double>(0,6);
            d.p_2 = intrinsic_cam.at<double>(0,7);
            d.k_3 = intrinsic_cam.at<double>(0,8);
        }
        else if(opt_params.refine_intrinsics_mode == REFINE_INTRINSICS_GUI && estimation.cams[cam_idx].refine_intrinsics){
            refine_idx = estimation.refine_idx[cam_idx];
            refine_intrinsics = true;

            Mat intrinsic_cam = intrinsics.colRange(9*refine_idx, 9*refine_idx+9);
            K.at<double>(0,0) = intrinsic_cam.at<double>(0,0);
            K.at<double>(1,1) = intrinsic_cam.at<double>(0,1);
            K.at<double>(0,2) = intrinsic_cam.at<double>(0,2);
            K.at<double>(1,2) = intrinsic_cam.at<double>(0,3);
            d.k_1 = intrinsic_cam.at<double>(0,4);
            d.k_2 = intrinsic_cam.at<double>(0,5);
            d.p_1 = intrinsic_cam.at<double>(0,6);
            d.p_2 = intrinsic_cam.at<double>(0,7);
            d.k_3 = intrinsic_cam.at<double>(0,8);
        }
        else{
            K = estimation.cams[cam_idx].K;
            d = estimation.cams[cam_idx].distortion;
        }

        Mat T_x_rig = detection.rig_positions[rig_idx].first*estimation.x_axis;
        Mat T_y_rig = detection.rig_positions[rig_idx].second*estimation.y_axis;

        if(refine_rig){
            T_x_rig = detection.rig_positions[rig_idx].first*rig.colRange(0,3).reshape(1,3);
            T_y_rig = detection.rig_positions[rig_idx].second*rig.colRange(3,6).reshape(1,3);
        }

        //calculate rotation matrices and derivatives of those wrt the rodrigues parameters
        Mat R_pattern, R_cam, dR_pattern_dRod_pattern, dR_cam_dRod_cam;
        cv::Rodrigues(Rod_pattern,R_pattern,dR_pattern_dRod_pattern);
        dR_pattern_dRod_pattern = dR_pattern_dRod_pattern.t();
        cv::Rodrigues(Rod_cam,R_cam,dR_cam_dRod_cam);
        dR_cam_dRod_cam = dR_cam_dRod_cam.t();

        //calculate helpful matrices
        Mat Id = Mat::eye(3,3,CV_64F);
        Mat df_dR_cam, dR_c_R_p_y_dR_c, dR_c_R_p_y_dR_p;
        Mat dR_c_T_p_dR_c = cv::Mat::zeros(3,9,CV_64F);
        for(int col = 0; col < 9; col++){
            int col_idx = (int) ((float)(col)/3.0);
            Id.col(col_idx).copyTo(dR_c_T_p_dR_c.col(col));
            dR_c_T_p_dR_c.col(col) *= T_pattern.at<double>(col%3,0);
        }

        Mat J_proj, J_dist, J_img;
        Mat df_dRod_cam_proj, df_dT_cam_proj, df_dRod_pattern_proj, df_dT_pattern_proj, df_ddistortion, df_dK, df_dT_x_rig, df_dT_y_rig;
        Mat x, y, f_y_proj, f_y, f_y_dist, f_y_img, proj_error, R_p_y;

        //calculate derivatives of 3D-to-camera projection f
        //1. df/dt_cam = Id
        Mat df_dT_cam =  Id;
        //2. df/dt_pattern = R_cam
        Mat df_dT_pattern =  R_cam;
        //3. rotational derivatives
        Mat df_dRod_cam, df_dRod_pattern;
        for(size_t i = 0; i < object_points.total(); i++){
            //get object point
            y = object_points.row(i);
            y = y.reshape(1,3);
            x = image_points.row(i);
            x = x.reshape(1,2);

            //calculate 3d projection coordinates f(y)
            f_y = R_cam*(R_pattern*y+T_pattern)+T_cam+T_x_rig+T_y_rig;

            //calculate 2d projection
            f_y_proj = Mat::zeros(2,1,CV_64F);
            f_y_proj.at<double>(0,0) = f_y.at<double>(0,0)/f_y.at<double>(2,0);
            f_y_proj.at<double>(1,0) = f_y.at<double>(1,0)/f_y.at<double>(2,0);

            //calculate Jacobian for 3d-to-2d projection
            J_proj = Mat::zeros(2,3,CV_64F);
            J_proj.at<double>(0,0) = 1.0/f_y.at<double>(2,0);
            J_proj.at<double>(1,1) = 1.0/f_y.at<double>(2,0);
            J_proj.at<double>(0,2) = -f_y.at<double>(0,0)/pow(f_y.at<double>(2,0),2.0);
            J_proj.at<double>(1,2) = -f_y.at<double>(1,0)/pow(f_y.at<double>(2,0),2.0);


            //distort projection
            double x_ = f_y_proj.at<double>(0,0);
            double y_ = f_y_proj.at<double>(1,0);
            double r_ = x_*x_+y_*y_;
            f_y_dist = Mat::zeros(2,1,CV_64F);
            f_y_dist.at<double>(0,0) = x_*(1+d.k_1*r_+d.k_2*r_*r_+d.k_3*r_*r_*r_)+2.0*d.p_1*x_*y_+d.p_2*(r_+2.0*x_*x_);
            f_y_dist.at<double>(1,0) = y_*(1+d.k_1*r_+d.k_2*r_*r_+d.k_3*r_*r_*r_)+2.0*d.p_2*x_*y_+d.p_1*(r_+2.0*y_*y_);

            //calculate Jacobian for distortion
            J_dist = Mat::zeros(2,2,CV_64F);
            J_dist.at<double>(0,0) = 1+d.k_1*(2.0*x_*x_+r_)+d.k_2*(r_*r_+4.0*x_*x_*r_)+d.k_3*(r_*r_*r_+6.0*x_*x_*r_*r_)+2.0*d.p_1*y_+6.0*x_*d.p_2;
            J_dist.at<double>(0,1) = 2.0*y_*d.k_1*x_+4.0*d.k_2*(x_*y_*r_)+6.0*d.k_3*(x_*y_*r_*r_)+2.0*d.p_1*x_+2.0*d.p_2*y_;
            J_dist.at<double>(1,0) = J_dist.at<double>(0,1);
            J_dist.at<double>(1,1) = 1+d.k_1*(2.0*y_*y_+r_)+d.k_2*(r_*r_+4.0*y_*y_*r_)+d.k_3*(r_*r_*r_+6.0*y_*y_*r_*r_)+2.0*d.p_2*x_+6.0*y_*d.p_1;

            //transfer to image coordinates
            f_y_img = Mat::zeros(2,1,CV_64F);
            f_y_img.at<double>(0,0) = K.at<double>(0,0)*f_y_dist.at<double>(0,0)+K.at<double>(0,2);
            f_y_img.at<double>(1,0) = K.at<double>(1,1)*f_y_dist.at<double>(1,0)+K.at<double>(1,2);

            //calculate Jacobian for coordinate change
            J_img = Mat::zeros(2,2,CV_64F);
            K.rowRange(0,2).colRange(0,2).copyTo(J_img);

            //calculate Jacobians for intrinsic refinement
            if(refine_intrinsics){
                Mat ddist_ddistortion = Mat::zeros(2,5,CV_64F);
                //ddist/dk_1
                ddist_ddistortion.at<double>(0,0) = x_*r_;
                ddist_ddistortion.at<double>(1,0) = y_*r_;
                //ddist/dk_2
                ddist_ddistortion.at<double>(0,1) = x_*r_*r_;
                ddist_ddistortion.at<double>(1,1) = y_*r_*r_;
                //ddist/dp_1
                ddist_ddistortion.at<double>(0,2) = 2.0*x_*y_;
                ddist_ddistortion.at<double>(1,2) = r_+2.0*y_*y_;
                //ddist/dp_2
                ddist_ddistortion.at<double>(0,3) = r_+2.0*x_*x_;
                ddist_ddistortion.at<double>(1,3) = 2.0*x_*y_;
                //ddist/dk_3
                ddist_ddistortion.at<double>(0,4) = x_*r_*r_*r_;
                ddist_ddistortion.at<double>(1,4) = y_*r_*r_*r_;

                df_ddistortion = J_img * ddist_ddistortion;

                df_dK = cv::Mat::zeros(2,4,CV_64F);
                //df/dfx
                df_dK.at<double>(0,0) = f_y_dist.at<double>(0,0);
                df_dK.at<double>(1,1) = f_y_dist.at<double>(1,0);
                df_dK.at<double>(0,2) = 1.0;
                df_dK.at<double>(1,3) = 1.0;
            }


            //save error
            proj_error = x-f_y_img;
            proj_error.copyTo(error->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2));

            //calculate helpful matrices which depend on the detected points
            R_p_y = R_pattern * y;
            dR_c_R_p_y_dR_c = cv::Mat::zeros(3,9,CV_64F);
            for(int col = 0; col < 9; col++){
                int col_idx = (int) ((float)(col)/3.0);
                Id.col(col_idx).copyTo(dR_c_R_p_y_dR_c.col(col));
                dR_c_R_p_y_dR_c.col(col) *= R_p_y.at<double>(col%3,0);
            }

            dR_c_R_p_y_dR_p = cv::Mat::zeros(3,9,CV_64F);
            for(int col = 0; col < 9; col++){
                int col_idx = (int) ((float)(col)/3.0);
                Id.col(col_idx).copyTo(dR_c_R_p_y_dR_p.col(col));
                dR_c_R_p_y_dR_p.col(col) *= y.at<double>(col%3,0);
            }

            //3.1. df/dRod_cam = df/dR_cam*dR_cam/dRod_cam = (dR_cam_R_pattern_y/dR_cam + dR_camT_pattern/dR_cam)*dR_cam/dRod_cam
            df_dR_cam = dR_c_R_p_y_dR_c+dR_c_T_p_dR_c;
            df_dRod_cam = df_dR_cam * dR_cam_dRod_cam;

            //3.2. df/dRod_pattern = df/dR_pattern*dR_pattern/dRod_pattern = dR_camR_pattern_y/dR_pattern*dR_pattern/dRod_pattern
            df_dRod_pattern = dR_c_R_p_y_dR_p * dR_pattern_dRod_pattern;

            //save derivatives to jacobian
            df_dRod_cam_proj = J_img*J_dist*J_proj*df_dRod_cam;
            df_dT_cam_proj = J_img*J_dist*J_proj*df_dT_cam;
            df_dRod_pattern_proj = J_img*J_dist*J_proj*df_dRod_pattern;
            df_dT_pattern_proj = J_img*J_dist*J_proj*df_dT_pattern;

            if(edge.cam_vertex > 0 && optimize_extrinsics){
                df_dRod_cam_proj.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange((optimization_index-1)*6, (optimization_index-1)*6 + 3));
                df_dT_cam_proj.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange((optimization_index-1)*6 + 3, (optimization_index-1)*6 + 6));
            }
            df_dRod_pattern_proj.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset+edge.pattern_vertex*6, offset+edge.pattern_vertex*6 + 3));
            df_dT_pattern_proj.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset+edge.pattern_vertex*6 + 3, offset+edge.pattern_vertex*6 + 6));

            if(refine_intrinsics){
                int offset_intr = extrinsics.total();
                df_dK.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset_intr+refine_idx*9, offset_intr+refine_idx*9+4));
                df_ddistortion.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset_intr+refine_idx*9+4, offset_intr+refine_idx*9+9));
            }

            if(refine_rig){
                int offset_rig = extrinsics.total()+intrinsics.total();
                df_dT_x_rig = detection.rig_positions[rig_idx].first*J_img*J_dist*J_proj*Mat::eye(3,3,CV_64F);
                df_dT_y_rig = detection.rig_positions[rig_idx].second*J_img*J_dist*J_proj*Mat::eye(3,3,CV_64F);
                df_dT_x_rig.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset_rig, offset_rig+3));
                df_dT_y_rig.copyTo(jacobian->rowRange(start_idx[edge_idx]+2*i,start_idx[edge_idx]+2*i+2).colRange(offset_rig+3, offset_rig+6));
            }
        }
    }

    return true;
}
