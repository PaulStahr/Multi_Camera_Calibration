#ifndef IO_H
#define IO_H
#pragma once

#include "data.h"

#include <fstream>

#include <QObject>

namespace Calibration{

class IO : public QObject
{
    Q_OBJECT
public:

    const int DELIM_UNDERSCORE = 0;
    const int DELIM_MINUS = 1;
    const int DELIM_BOTH = 2;

    const int READ_FEATURES = 0;
    const int READ_RESOLUTION = 1;
    const int READ_RIG_POSITIONS = 2;

    ///
    /// \brief IO is the standard constructor
    /// \param parent
    ///
    explicit IO(QObject *parent = nullptr);

    ///
    /// \brief saveParameters
    /// \param file_path
    /// \param parameters
    /// \return
    ///
    bool saveParameters(const std::string& file_path, const std::vector<Parameter>& parameters);

    ///
    /// \brief loadParameters
    /// \param file_path
    /// \param parameters
    /// \return
    ///
    bool loadParameters(const std::string& file_path, std::vector<Parameter>* parameters);

    ///
    /// \brief loadRigPositions loads a file containing the rig position parameters
    /// \param file_path             path to the configuration file
    /// \param num_rig_positions    counter for the number of unique rig positions
    /// \param rig_positions        output of all rig positions read from file accessable by the rig position ids
    /// \return                     true if successful, false if file does not exist or is incorrectly formatted
    ///
    bool loadRigPositions(const std::string& file_path, int* num_rig_positions, std::map<int,pairf>* rig_positions);

    ///
    /// \brief createImageList saves a list of all images available in the specified directory and analyzes the file names as well as rig position files
    /// \param dir_path         path to image input directory
    /// \param rig_positions    all rig positions read from file accessable by the rig position ids
    /// \param file_list        output file lists and corresponding index list
    /// \param detection        contains number of cameras, pattern and rig positions
    /// \param cameras          list of cameras to save the resolutions to
    /// \return                 true if list was successfully created
    ///
    bool createImageList(const std::string& dir_path, const std::map<int, pairf>& rig_positions, Files* file_list , Detection* detection, std::vector<Camera>* cameras);

    ///
    /// \brief saveCorrespondences saves the given detected patterns as well as the rig positions to a file at the specified path
    /// \param file_path            to the file to save
    /// \param correspondences      table of detected point lists
    /// \param rig_positions        table of loaded rig positions
    /// \param cameras              list of cameras containing the resolutions to save with the correspondences list
    /// \return                     true if save is successful, false otherwise, e.g. in the case the file_path is not valid
    ///
    bool saveCorrespondences(const std::string& file_path, const vector_3D<Feature_List>& correspondences, const std::vector<pairf>& rig_positions, const std::vector<Camera>& cameras);

    ///
    /// \brief loadCorrespondences loads previously saved detected pattern points and rig positions
    /// \param file_path            file to load the data from
    /// \param detection            contains number of cameras, pattern and rig positions
    /// \param cameras              camera list to save new cameras to
    /// \return                     true if successful, false otherwise e.g. if the file does not exist or is ill-formatted
    ///
    bool loadCorrespondences(const std::string& file_path, Detection* detection, Estimation *estimation);

    ///
    /// \brief saveCameras saves the given camera parameters to a file at the specified path
    /// \param file_path            to the file to save
    /// \param cameras              parameter list to save to
    /// \return                     true if save is successful, false otherwise, e.g. in the case the file_path is not valid
    ///
    bool saveCameras(const std::string& file_path, const std::vector<Camera>& cameras);

    ///
    /// \brief loadCameras loads previously saved cameras
    /// \param file_path            file to load the data from
    /// \param cameras              parameter list to load to
    /// \return                     true if successful, false otherwise e.g. if the file does not exist or is ill-formatted
    ///
    bool loadCameras(const std::string& file_path, std::vector<Camera>* cameras);

    ///
    /// \brief saveTransformations saves the rotations and translations of estimation
    /// \param file_path            to the file to save
    /// \param estimation           struct which contains the rotations and translations
    /// \return                     true if save is successful, false otherwise, e.g. in the case the file_path is not valid
    ///
    bool saveTransformations(const std::string& file_path, const Estimation& estimation);

    ///
    /// \brief loadTransformations loads the rotations and translations from the specified file to the estimation
    /// \param file_path            file to load the data from
    /// \param estimation           struct which contains the rotations and translations
    /// \return                     true if successful, false otherwise e.g. if the file does not exist or is ill-formatted
    ///
    bool loadTransformations(const std::string& file_path, Estimation* estimation);

    ///
    /// \brief readFloatImage
    /// \param file_path
    /// \param image
    /// \return
    ///
    bool readFloatImage(const std::string file_path, cv::Mat* image);

private:

    ///
    /// \brief splitString splits the given string into integers according to the delimiter policy
    /// \param str          string to split
    /// \param parts        parts of the string
    /// \param delim_code   specifies the characters which are allowed as delimiters
    /// \return             true if successful, false if string parts are not integers
    ///
    bool splitString(const std::string& str, std::vector<std::string>* parts, const int delim_code);

    ///
    /// \brief saveMat
    /// \param file
    /// \param mat
    /// \return
    ///
    bool saveMat(std::ofstream* file, const cv::Mat& mat);

    ///
    /// \brief loadMat
    /// \param file
    /// \param mat
    /// \return
    ///
    bool loadMat(std::istringstream *file, cv::Mat* mat);


signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);


};
}
#endif // IO_H
