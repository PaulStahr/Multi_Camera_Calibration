#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H
#pragma once

#include "data.h"

#include <QObject>

namespace Calibration{

class Optimization : public QObject
{
    Q_OBJECT
public:
    explicit Optimization(QObject *parent = nullptr);

    ///
    /// \brief intrinsicsEstimation
    /// \param detection
    /// \param estimation
    /// \param initial_intrinsics_mode
    /// \return
    ///
    bool intrinsicsEstimation(const Detection& detection, Estimation* estimation, const int& initial_intrinsics_mode = INIT_INTRINSICS_NONE);

    ///
    /// \brief optimizeExtrinsics
    /// \param detection
    /// \param estimation
    /// \param params
    /// \param opt_params
    /// \return
    ///
    bool optimizeExtrinsics(const Detection& detection, Estimation* estimation, const LM_Params& params, const Optimization_Params& opt_params);

public slots:
    ///
    /// \brief singleIntrinsicsEstimation
    /// \param detection
    /// \param estimation
    /// \param use_initial_intrinsics
    /// \param cam_idx_est
    /// \param cam_idx_det
    /// \param cam
    /// \param rms
    /// \param counter
    ///
    void singleIntrinsicsEstimation(const Detection& detection, Estimation* estimation, const bool& use_initial_intrinsics, const int& cam_idx_est, const int& cam_idx_det, Camera* cam, double* rms, Safe_Counter* counter);

private:

    ///
    /// \brief buildGraph
    /// \param detection
    /// \param estimation
    /// \param initial_extrinsics_mode
    /// \param graph
    /// \return
    ///
    bool buildGraph(const Detection& detection, Estimation* estimation, const int& initial_extrinsics_mode, Graph* graph);

    ///
    /// \brief optimizeExtrinsicsLM
    /// \param detection
    /// \param estimation
    /// \param graph
    /// \param error
    /// \param params
    /// \param opt_params
    /// \return
    ///
    bool optimizeExtrinsicsLM(const Detection& detection, Estimation* estimation, Graph* graph, double* error, const LM_Params& params, const Optimization_Params& opt_params);

    ///
    /// \brief calculateJacobian
    /// \param detection
    /// \param estimation
    /// \param extrinsics
    /// \param intrinsics
    /// \param opt_params
    /// \param rig
    /// \param graph
    /// \param jacobian
    /// \param error
    /// \return
    ///
    bool calculateJacobian(const Detection& detection, const Estimation& estimation, const cv::Mat& extrinsics, const cv::Mat& intrinsics, const Optimization_Params& opt_params, const cv::Mat& rig, const Calibration::Graph& graph, cv::Mat* jacobian, cv::Mat* error);


signals:
    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(QString msg, cv::Mat mat);

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);

    ///
    /// \brief startIntrinsicsThread
    /// \param detection
    /// \param estimation
    /// \param use_initial_intrinsics
    /// \param cam_idx_est
    /// \param cam_idx_det
    /// \param cam
    /// \param rms
    /// \param counter
    ///
    void startIntrinsicsThread(const Detection& detection, Estimation* estimation, const bool& use_initial_intrinsics, const int& cam_idx_est, const int& cam_idx_det, Camera* cam, double* rms, Safe_Counter* counter);

    ///
    /// \brief isFinished
    ///
    void isFinished();

};
}

#endif // OPTIMIZATION_H
