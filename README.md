# Multi Camera Calibration
Toolkit for the calibration of a system consisting of multiple cameras, possibly mounted onto a 2 axes rig as used in [our light field research](https://www.mip.informatik.uni-kiel.de/en/research/light-field-capturing). The calibration is based on ChArUco boards, a combination of the classical checkerboard and ArUco markers, which allows the calibration pattern to be partly occluded.

![GUI Overview](https://gitlab.com/ungetym/Multi_Camera_Calibration/raw/master/images/tutorial_basic_50.png "GUI Overview")

Requires:
- [QT 5](https://www.qt.io/download-qt-installer)
- OpenCV 3
- Compiler with C++14 support (e.g. gcc)

Build instructions:
- Currently we only use qmake within the QtCreator (a cmake config might be provided later), so simply open the project in QtCreator
- Modify the .pro file to set the correct OpenCV path and if your OpenCV installation is not system-wide known also include the opencv header path
- You should now be able to build the tool

Some instructions on the usage of this tool are given in [this repository's Wiki](https://gitlab.com/ungetym/Multi_Camera_Calibration/wikis/Basic-Workflow).

For further information contact me via <tmi@informatik.uni-kiel.de>.